package com.grihachikitsa.grihachikitsa.Views.WizardViewPager;

/**
 * Created by Nikil on 9/20/2016.
 */
public interface ModelCallbacks {
    void onPageDataChanged(Page page);
    void onPageTreeChanged();
}