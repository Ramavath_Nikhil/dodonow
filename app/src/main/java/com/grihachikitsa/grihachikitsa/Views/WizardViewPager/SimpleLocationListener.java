package com.grihachikitsa.grihachikitsa.Views.WizardViewPager;

/**
 * Created by Nikil on 9/20/2016.
 */
import android.location.Location;

public interface SimpleLocationListener {
    public void onLocationChanged(Location location);
}