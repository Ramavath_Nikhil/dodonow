package com.grihachikitsa.grihachikitsa.Views.WizardViewPager;

import android.support.v4.app.Fragment;

import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.ui.ImageFragment;


/**
 * Created by Nikil on 9/20/2016.
 */
public class ImagePage extends TextPage {

    public ImagePage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return ImageFragment.create(getKey());
    }

    public ImagePage setValue(String value) {
        mData.putString(SIMPLE_DATA_KEY, value);
        return this;
    }
}