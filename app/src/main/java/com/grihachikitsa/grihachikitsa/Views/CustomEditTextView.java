package com.grihachikitsa.grihachikitsa.Views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by nikhi on 1/22/2016.
 */
public class CustomEditTextView extends EditText {



    public CustomEditTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEditTextView(Context context) {
        super(context);
        init();
        this.clearComposingText();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(),  "fonts/Bariol_Regular.otf");
            setTypeface(tf);
        }
    }

}