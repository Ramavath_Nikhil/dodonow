package com.grihachikitsa.grihachikitsa.Views.WizardViewPager;

import android.support.v4.app.Fragment;

import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.ui.GeoFragment;


/**
 * Created by Nikil on 9/20/2016.
 */
public class GeoPage extends TextPage {

    public GeoPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return GeoFragment.create(getKey());
    }

    public GeoPage setValue(String value) {
        mData.putString(SIMPLE_DATA_KEY, value);
        return this;
    }
}