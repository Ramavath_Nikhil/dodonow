package com.grihachikitsa.grihachikitsa.Views.WizardViewPager;

/**
 * Created by Nikil on 9/20/2016.
 */
import android.support.v4.app.Fragment;

import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.ui.NumberFragment;


public class NumberPage extends TextPage {

    public NumberPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return NumberFragment.create(getKey());
    }

}