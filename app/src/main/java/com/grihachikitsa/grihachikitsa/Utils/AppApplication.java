package com.grihachikitsa.grihachikitsa.Utils;

/**
 * Created by Nikil on 12/24/2016.
 */
import android.app.Application;
import android.content.ContextWrapper;
import android.support.multidex.MultiDexApplication;


public class AppApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize the Prefs class
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }
}