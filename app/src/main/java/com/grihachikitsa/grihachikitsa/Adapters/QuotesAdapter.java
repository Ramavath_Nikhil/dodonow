package com.grihachikitsa.grihachikitsa.Adapters;

/**
 * Created by Nikil on 12/6/2016.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.grihachikitsa.grihachikitsa.Activites.MainActivity;
import com.grihachikitsa.grihachikitsa.Activites.QuotesActivity;
import com.grihachikitsa.grihachikitsa.Models.GetQuotesModel;
import com.grihachikitsa.grihachikitsa.Models.OngoingProjectsModel;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.CareTakerCompleteDetailsModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.LoginResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Config;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;
import com.grihachikitsa.grihachikitsa.Views.CustomProgressDialog;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuotesAdapter extends RecyclerView.Adapter<QuotesAdapter.MyViewHolder> {

    private List<GetQuotesModel> ongoingProjectsModelList;
    private Context context;
    private QuotesActivity quotesActivity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView organisationName, priceQuoted, postedOn;
        public Button viewDetails, book;

        public MyViewHolder(View view) {
            super(view);

            organisationName = (TextView) view.findViewById(R.id.organisationName);
            priceQuoted = (TextView) view.findViewById(R.id.priceQuoted);
            postedOn = (TextView) view.findViewById(R.id.postedOn);
            viewDetails = (Button) view.findViewById(R.id.viewDetails);
            book = (Button) view.findViewById(R.id.book);
        }
    }


    public QuotesAdapter(List<GetQuotesModel> moviesList, Context context, QuotesActivity quotesActivity) {
        this.ongoingProjectsModelList = moviesList;
        this.context = context;
        this.quotesActivity = quotesActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_quotes, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final GetQuotesModel getQuotesModel = ongoingProjectsModelList.get(position);
        holder.organisationName.setText(checkNull(getQuotesModel.getOrganisation_name()));
        holder.priceQuoted.setText("Coated price: " + checkNull(getQuotesModel.getCoated_price()));
        holder.postedOn.setText("Posted on :" + checkNull(converDateTime(getQuotesModel.getCreated_at())));
        holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCareTakerDetails(getQuotesModel.getCaretaker_id(),getQuotesModel.getProject_id(), getQuotesModel.getCaretaker_id(), getQuotesModel.getCoated_price());
            }
        });

        holder.book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bookCareTaker(getQuotesModel.getProject_id(), getQuotesModel.getCaretaker_id(), getQuotesModel.getCoated_price());
            }
        });

    }

    public void getCareTakerDetails(String id, final String project_id, final String caretakter_id, final String coated_price) {
        final CustomProgressDialog progressBar = CustomProgressDialog.show(context);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<CareTakerCompleteDetailsModel> call = apiService.getCareTakerCompleteDetails(id);
        call.enqueue(new Callback<CareTakerCompleteDetailsModel>() {
            @Override
            public void onResponse(Call<CareTakerCompleteDetailsModel> call, Response<CareTakerCompleteDetailsModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                        progressBar.cancel();
                        printUserDetails(response.body().getTasks().get(0),project_id,caretakter_id,coated_price);
                    } else {
                        progressBar.cancel();
                    }

                } else {
                    progressBar.cancel();

                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<CareTakerCompleteDetailsModel> call, Throwable t) {

                t.printStackTrace();
                progressBar.cancel();

            }


        });


    }

    public void printUserDetails(CareTakerCompleteDetailsModel careTakerCompleteDetailsModel, final String project_id, final String caretakter_id, final String coated_price) {
        if (careTakerCompleteDetailsModel != null) {
            TextView first_name, last_name, email, mobile, gender, age, doj, qualification, experience, designation, address, residenceNumber, id_addressProof, bloodGroup, father, mother, spouse, martialStatus, childre, backgroundVerification;
            Button ok,bookCareTaker;

            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.layout_popup_caretakerdetails);
            CircleImageView profilePic = (CircleImageView) dialog.findViewById(R.id.profilepic);
            first_name = (TextView) dialog.findViewById(R.id.firstname);

            email = (TextView) dialog.findViewById(R.id.email);
            mobile = (TextView) dialog.findViewById(R.id.mobile);
            gender = (TextView) dialog.findViewById(R.id.gender);
            age = (TextView) dialog.findViewById(R.id.age);
            doj = (TextView) dialog.findViewById(R.id.doj);
            qualification = (TextView) dialog.findViewById(R.id.qualification);
            experience = (TextView) dialog.findViewById(R.id.experience);
            designation = (TextView) dialog.findViewById(R.id.designation);
            residenceNumber = (TextView) dialog.findViewById(R.id.residncenumber);
            id_addressProof = (TextView) dialog.findViewById(R.id.addressProof);
            bloodGroup = (TextView) dialog.findViewById(R.id.bloodgroup);
            father = (TextView) dialog.findViewById(R.id.father);
            mother = (TextView) dialog.findViewById(R.id.mother);
            martialStatus = (TextView) dialog.findViewById(R.id.martial_status);
            spouse = (TextView) dialog.findViewById(R.id.spouse);
            childre = (TextView) dialog.findViewById(R.id.children);
            backgroundVerification = (TextView) dialog.findViewById(R.id.backgroundVerification);
            address = (TextView) dialog.findViewById(R.id.address);

            //BUTTON ON CLICK LISTENER
            ok = (Button) dialog.findViewById(R.id.ok);
            bookCareTaker = (Button) dialog.findViewById(R.id.bookCareTaker);

            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
            bookCareTaker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bookCareTaker(project_id,caretakter_id,coated_price);
                }
            });

            //CIRCULAT IMAGE VIEW


            first_name.setText("Name: " + checkNull(careTakerCompleteDetailsModel.getCaretaker_name()));
            email.setText("Email: " + checkNull(careTakerCompleteDetailsModel.getEmail()));
            mobile.setText("Mobile: " + checkNull(careTakerCompleteDetailsModel.getMobile()));
            gender.setText("Gender: " + checkNull(careTakerCompleteDetailsModel.getGender()));
            age.setText("Age: " + checkNull(careTakerCompleteDetailsModel.getAge()));
            doj.setText("Date of joinging: " + checkNull(careTakerCompleteDetailsModel.getDOJ()));
            qualification.setText("Qualification: " + checkNull(careTakerCompleteDetailsModel.getQualification()));
            experience.setText("Experience: " + checkNull(careTakerCompleteDetailsModel.getExperience()));
            designation.setText("Designation: " + checkNull(careTakerCompleteDetailsModel.getDesignation()));
            residenceNumber.setText("Residence number: " + checkNull(careTakerCompleteDetailsModel.getResidence_number()));
            id_addressProof.setText("ID/Address proof: " + checkNull(careTakerCompleteDetailsModel.getId_addressProof()));
            bloodGroup.setText("Blood group: " + checkNull(careTakerCompleteDetailsModel.getBlood_group()));
            father.setText("Father: " + checkNull(careTakerCompleteDetailsModel.getFather()));
            mother.setText("Mother: " + checkNull(careTakerCompleteDetailsModel.getMother()));
            spouse.setText("Spouse: " + checkNull(careTakerCompleteDetailsModel.getSpouse()));
            childre.setText("Children: " + checkNull(careTakerCompleteDetailsModel.getChildren()));
            address.setText("Address: " + checkNull(careTakerCompleteDetailsModel.getAddress()));
            martialStatus.setText("Martial status: " + checkNull(careTakerCompleteDetailsModel.getMartial_status()));

            if (careTakerCompleteDetailsModel.getBackground_verification() != null && !careTakerCompleteDetailsModel.getBackground_verification().isEmpty()) {
                if (careTakerCompleteDetailsModel.getBackground_verification().equalsIgnoreCase("1"))
                    backgroundVerification.setText("Background verification: " + "Verified");
                else if (careTakerCompleteDetailsModel.getBackground_verification().equalsIgnoreCase("0"))
                    backgroundVerification.setText("Background verification: " + "Not Verified");
            }
            Picasso.with(context).load(Config.BASE_URL + careTakerCompleteDetailsModel.getProfile_pic()).placeholder(R.drawable.ic_person).error(R.drawable.ic_person).into(profilePic);

            dialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

            lp.copyFrom(dialog.getWindow().getAttributes());
            DisplayMetrics displaymetrics = new DisplayMetrics();
            quotesActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int height = displaymetrics.heightPixels;

            int width = displaymetrics.widthPixels;


            dialog.getWindow().setLayout(width, lp.height);
        }
    }

    public String checkNull(String s) {
        if (s == null || s.isEmpty())
            return "N/A";
        else return s;
    }

    public String converDateTime(String dateTime) {
        try {


            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = f.parse(dateTime);
            String day = (String) android.text.format.DateFormat.format("dd", date);
            String stringMonth = (String) android.text.format.DateFormat.format("MMM", date);
            String hours = (String) android.text.format.DateFormat.format("HH", date);
            String minutes = (String) android.text.format.DateFormat.format("mm", date);
            Log.d("time", date + "");
            return day + " " + stringMonth + ", " + hours + ":" + minutes;
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public int getItemCount() {
        return ongoingProjectsModelList.size();
    }

    public void updateRefernceStatus() {

    }


    public void bookCareTaker(String project_id, String caretakter_id, String coated_price) {

        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<LoginResponseModel> call = apiService.bookCareTaker(Prefs.getString("apiKey", ""), project_id, caretakter_id, coated_price);
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {

                if (response.body() != null && response.body().getError()!=null && response.body().getError().equalsIgnoreCase("false")) {
                    Log.d("Response", response.body().toString() + "");
                    customProgressDialog.cancel();
                    Toast.makeText(context, "Booking confirmed", Toast.LENGTH_SHORT).show();
                    context.startActivity(new Intent(context, MainActivity.class));


                } else {
                    customProgressDialog.cancel();
                    if (response.body() != null && response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                customProgressDialog.cancel();
            }


        });
    }

}

