package com.grihachikitsa.grihachikitsa.Adapters;

/**
 * Created by Nikil on 12/6/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.grihachikitsa.grihachikitsa.Activites.ProductCompleteDetailsActivity;
import com.grihachikitsa.grihachikitsa.Activites.QuotesActivity;
import com.grihachikitsa.grihachikitsa.Models.OngoingProjectsModel;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProjectCompletDetailsModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OngoingProjectsAdapter extends RecyclerView.Adapter<OngoingProjectsAdapter.MyViewHolder> {

    private List<OngoingProjectsModel> ongoingProjectsModelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView projectType, projectId, postedDate, quotesCount;
        public Button viewQuotes;

        public MyViewHolder(View view) {
            super(view);
            projectType = (TextView) view.findViewById(R.id.projectType);
            projectId = (TextView) view.findViewById(R.id.projectID);
            postedDate = (TextView) view.findViewById(R.id.date);
            quotesCount = (TextView) view.findViewById(R.id.quotesCount);
            viewQuotes = (Button) view.findViewById(R.id.viewQuotes);

        }
    }


    public OngoingProjectsAdapter(List<OngoingProjectsModel> moviesList, Context context) {
        this.ongoingProjectsModelList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_ongoingprojects, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final OngoingProjectsModel ongoingProjectsModel = ongoingProjectsModelList.get(position);
        //  holder.title.setText("You took a ride on "+movie.getCreated_at());

        holder.projectId.setText("Project ID: " + checkNull(ongoingProjectsModel.getProject_id()));
        holder.projectType.setText(checkNull(ongoingProjectsModel.getProject_type()));
        holder.postedDate.setText("Posted Date: " + checkNull(converDateTime(ongoingProjectsModel.getPosted_date())));

        if ((ongoingProjectsModel.getCaretaker_name() != null && !ongoingProjectsModel.getCaretaker_name().isEmpty() || ongoingProjectsModel.getCount().equalsIgnoreCase("0"))) {
            if (ongoingProjectsModel.getCaretaker_name() != null && !ongoingProjectsModel.getCaretaker_name().isEmpty()) {
                holder.quotesCount.setVisibility(View.VISIBLE);
                holder.quotesCount.setText("CareTaker: " + ongoingProjectsModel.getCaretaker_name());
            }
            else
            {
                holder.quotesCount.setVisibility(View.GONE);
            }

            holder.viewQuotes.setText("View Details");
        } else {
            holder.quotesCount.setText(checkNull(ongoingProjectsModel.getCount()) + " quotes proposed");
            holder.viewQuotes.setText("View Quotes");
        }
        holder.viewQuotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ((ongoingProjectsModel.getCaretaker_name() != null && !ongoingProjectsModel.getCaretaker_name().isEmpty()) || ongoingProjectsModel.getCount().equalsIgnoreCase("0")) {
                    context.startActivity(new Intent(context, ProductCompleteDetailsActivity.class
                    ).putExtra("project_id", ongoingProjectsModel.getProject_id()));
                } else {
                    context.startActivity(new Intent(context, QuotesActivity.class
                    ).putExtra("projectObject", ongoingProjectsModel));
                }
            }
        });
    }

    public String checkNull(String s) {
        if (s == null || s.isEmpty())
            return "N/A";
        else
            return s;
    }

    public String converDateTime(String dateTime) {
        try {


            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = f.parse(dateTime);
            String day = (String) android.text.format.DateFormat.format("dd", date);
            String stringMonth = (String) android.text.format.DateFormat.format("MMM", date);
            String hours = (String) android.text.format.DateFormat.format("HH", date);
            String minutes = (String) android.text.format.DateFormat.format("mm", date);
            Log.d("time", date + "");
            return day + " " + stringMonth + ", " + hours + ":" + minutes;
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public int getItemCount() {
        return ongoingProjectsModelList.size();
    }

    public void updateRefernceStatus() {

    }

}

