package com.grihachikitsa.grihachikitsa.Adapters;

/**
 * Created by Nikil on 12/6/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.grihachikitsa.grihachikitsa.Activites.ProductCompleteDetailsActivity;
import com.grihachikitsa.grihachikitsa.Activites.QuotesActivity;
import com.grihachikitsa.grihachikitsa.Models.OngoingProjectsModel;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.OrdersHistoryModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProjectCompletDetailsModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder> {

    private List<OrdersHistoryModel> ongoingProjectsModelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView orderID, productStatus, paymentType, totalPrice, placedOn;
        public ImageView productImage;

        public MyViewHolder(View view) {
            super(view);

            orderID = (TextView) view.findViewById(R.id.orderid);
            productStatus = (TextView) view.findViewById(R.id.status);
            paymentType = (TextView) view.findViewById(R.id.paymentType);
            totalPrice = (TextView) view.findViewById(R.id.price);
            placedOn = (TextView) view.findViewById(R.id.placedon);


        }
    }


    public OrderHistoryAdapter(List<OrdersHistoryModel> moviesList, Context context) {
        this.ongoingProjectsModelList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_orderhistory, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final OrdersHistoryModel ongoingProjectsModel = ongoingProjectsModelList.get(position);
        //  holder.title.setText("You took a ride on "+movie.getCreated_at());
        holder.orderID.setText("Order ID: " + checkNull(ongoingProjectsModel.getId()));
        holder.productStatus.setText("Status: " + checkNull(ongoingProjectsModel.getStatus()));
        holder.paymentType.setText("Payment type: " + checkNull(ongoingProjectsModel.getPayment_type()));
        holder.placedOn.setText("Placed on: " + checkNull(converDateTime(ongoingProjectsModel.getCreated_at())));
        holder.totalPrice.setText("Total Price: " + checkNull(ongoingProjectsModel.getTotal_price()));
    }

    public String checkNull(String s) {
        if (s == null || s.isEmpty())
            return "N/A";
        else
            return s;
    }

    public String converDateTime(String dateTime) {
        try {


            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = f.parse(dateTime);
            String day = (String) android.text.format.DateFormat.format("dd", date);
            String stringMonth = (String) android.text.format.DateFormat.format("MMM", date);
            String hours = (String) android.text.format.DateFormat.format("HH", date);
            String minutes = (String) android.text.format.DateFormat.format("mm", date);
            Log.d("time", date + "");
            return day + " " + stringMonth + ", " + hours + ":" + minutes;
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public int getItemCount() {
        return ongoingProjectsModelList.size();
    }

    public void updateRefernceStatus() {

    }

}

