package com.grihachikitsa.grihachikitsa.Adapters;

/**
 * Created by Nikil on 12/6/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.grihachikitsa.grihachikitsa.Activites.CartActivity;
import com.grihachikitsa.grihachikitsa.Activites.ProductListingActivity;
import com.grihachikitsa.grihachikitsa.Activites.QuotesActivity;
import com.grihachikitsa.grihachikitsa.Models.OngoingProjectsModel;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.AddToCartResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProductsResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Config;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;
import com.grihachikitsa.grihachikitsa.Views.CustomProgressDialog;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {

    private List<ProductsResponseModel> ongoingProjectsModelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        /*  public TextView projectType, projectId, postedDate, quotesCount;
          public Button viewQuotes;*/
        public Button incrementCount, decrementCount;
        public int itemCount;
        public ImageView productImage;
        public TextView itemCountTextview, productName, productDescription, productPrice;

        public MyViewHolder(View view) {
            super(view);

            incrementCount = (Button) view.findViewById(R.id.increase);
            decrementCount = (Button) view.findViewById(R.id.decrease);
            itemCountTextview = (TextView) view.findViewById(R.id.itemcount);
            productName = (TextView) view.findViewById(R.id.productName);
            productDescription = (TextView) view.findViewById(R.id.productDescription);
            productPrice = (TextView) view.findViewById(R.id.productPrice);
            productImage = (ImageView) view.findViewById(R.id.productImage);
        }
    }


    public ProductsAdapter(List<ProductsResponseModel> moviesList, Context context) {
        this.ongoingProjectsModelList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_products, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(ongoingProjectsModelList.get(position).getId());
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final ProductsResponseModel productsResponseModel = ongoingProjectsModelList.get(position);

        holder.productName.setText(checkNull(productsResponseModel.getName()));
        holder.productPrice.setText(checkNull(productsResponseModel.getPrice()));
        holder.productDescription.setText(checkNull(productsResponseModel.getDescription()));
        if (productsResponseModel.getItem_count() != null && !productsResponseModel.getItem_count().isEmpty()) {
            holder.itemCount = Integer.parseInt(productsResponseModel.getItem_count());
            holder.itemCountTextview.setText(holder.itemCount + "");
        } else {
            holder.itemCount = 0;
            holder.itemCountTextview.setText(holder.itemCount + "");
        }
        Picasso.with(context)
                .load(Config.BASE_URL + productsResponseModel.getProduct_image())
                .placeholder(R.mipmap.ic_gclogo)
                .error(R.mipmap.ic_gclogo)
                .into(holder.productImage);
        holder.incrementCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                addItemToCart(productsResponseModel.getId(), holder, productsResponseModel);


            }
        });

        holder.decrementCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.itemCount > 0) {

                    removeItemFromCart(productsResponseModel.getId(), holder, productsResponseModel);
                } else {
                    Toast.makeText(context, "Please add atleast one item", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void addItemToCart(String product_id, final MyViewHolder holder, final ProductsResponseModel productsResponseModel) {

        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<AddToCartResponseModel> call = apiService.addItemToCart(Prefs.getString("apiKey", ""), product_id);
        call.enqueue(new Callback<AddToCartResponseModel>() {
            @Override
            public void onResponse(Call<AddToCartResponseModel> call, Response<AddToCartResponseModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {
                    Log.d("Response", response.body().toString() + "");
                    customProgressDialog.cancel();

                    holder.itemCount++;
                    holder.itemCountTextview.setText(holder.itemCount + "");
                    productsResponseModel.setItem_count(holder.itemCount + "");

                    if (context instanceof ProductListingActivity) {
                        // handle activity case
                        ((ProductListingActivity) context).onClickIncrementCartCount();
                    }


                } else {
                    customProgressDialog.cancel();
                    if (checkNull(response.body().getMessage()).isEmpty())
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(context, checkNull(response.body().getMessage()), Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call<AddToCartResponseModel> call, Throwable t) {

                t.printStackTrace();
                customProgressDialog.cancel();
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();

            }


        });

    }

    public void removeItemFromCart(String product_id, final MyViewHolder holder, final ProductsResponseModel productsResponseModel) {

        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<AddToCartResponseModel> call = apiService.updateCartCountbyRemovingOneItem(Prefs.getString("apiKey", ""), product_id);
        call.enqueue(new Callback<AddToCartResponseModel>() {
            @Override
            public void onResponse(Call<AddToCartResponseModel> call, Response<AddToCartResponseModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {
                    Log.d("Response", response.body().toString() + "");
                    customProgressDialog.cancel();



                    holder.itemCount--;
                    holder.itemCountTextview.setText(holder.itemCount + "");
                    productsResponseModel.setItem_count(holder.itemCount + "");

                    if (context instanceof ProductListingActivity)
                        ((ProductListingActivity) context).onClickDecrementCartCount();
                    else if (context instanceof CartActivity)
                    {

                        if(holder.itemCount<=0)
                        {
                           ongoingProjectsModelList.remove(productsResponseModel);
                            notifyDataSetChanged();
                        }
                        ((CartActivity) context).checkCartCount();
                    }

                } else {
                    customProgressDialog.cancel();
                    if (checkNull(response.body().getMessage()).isEmpty())
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(context, checkNull(response.body().getMessage()), Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call<AddToCartResponseModel> call, Throwable t) {

                t.printStackTrace();
                customProgressDialog.cancel();
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();

            }


        });

    }


    public String checkNull(String s) {
        if (s == null || s.isEmpty())
            return "N/A";
        else
            return s;
    }

    public String converDateTime(String dateTime) {
        try {


            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = f.parse(dateTime);
            String day = (String) android.text.format.DateFormat.format("dd", date);
            String stringMonth = (String) android.text.format.DateFormat.format("MMM", date);
            String hours = (String) android.text.format.DateFormat.format("HH", date);
            String minutes = (String) android.text.format.DateFormat.format("mm", date);
            Log.d("time", date + "");
            return day + " " + stringMonth + ", " + hours + ":" + minutes;
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public int getItemCount() {
        return ongoingProjectsModelList.size();
    }

    public void updateRefernceStatus() {

    }

}

