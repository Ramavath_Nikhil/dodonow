package com.grihachikitsa.grihachikitsa.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.grihachikitsa.grihachikitsa.Activites.CheckoutActivity;
import com.grihachikitsa.grihachikitsa.Activites.MainActivity;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.CareTakerCompleteDetailsModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProductsResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.RegisterResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Config;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;
import com.grihachikitsa.grihachikitsa.Views.CustomProgressDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nikil on 1/3/2017.
 */
public class CheckoutAdapter extends RecyclerView.Adapter<CheckoutAdapter.MyViewHolder> {

    private List<ProductsResponseModel> checkoutList;
    private Context context;
    private CheckoutActivity quotesActivity;
    private double totalPrice, error_count;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView productName, productQuantity, productPrice, error;
        public ImageView productImage, cancel;

        public MyViewHolder(View view) {
            super(view);
            productName = (TextView) view.findViewById(R.id.productName);
            productImage = (ImageView) view.findViewById(R.id.productImage);
            cancel = (ImageView) view.findViewById(R.id.cancel);
            productPrice = (TextView) view.findViewById(R.id.productPrice);
            productQuantity = (TextView) view.findViewById(R.id.productQuantity);
            error = (TextView) view.findViewById(R.id.error);

        }
    }

    public CheckoutAdapter(List<ProductsResponseModel> checkoutList, Context context, CheckoutActivity quotesActivity) {
        this.checkoutList = checkoutList;
        this.context = context;
        this.quotesActivity = quotesActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_checkout, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        try {
            final ProductsResponseModel productsResponseModel = checkoutList.get(position);
            holder.productName.setText(checkNUll(productsResponseModel.getName()));
            holder.productQuantity.setText("Quantity: " + productsResponseModel.getItem_count());
            Picasso.with(context).load(Config.BASE_URL + productsResponseModel.getProduct_image()).error(R.mipmap.ic_gclogo).into(holder.productImage);
            if (productsResponseModel.getStock().equalsIgnoreCase("0")) {
                holder.error.setVisibility(View.VISIBLE);
                holder.error.setText("Product is out of stock");
                error_count = error_count + 1;
            } else if (Integer.parseInt(productsResponseModel.getStock()) < Integer.parseInt(productsResponseModel.getItem_count())) {
                holder.error.setVisibility(View.VISIBLE);

                if (Integer.parseInt(productsResponseModel.getStock()) == 1) {
                    holder.error.setText("Only " + productsResponseModel.getStock() + " product is available");
                    error_count = error_count + 1;
                } else {
                    holder.error.setText("Only " + productsResponseModel.getStock() + " product are available");
                    error_count = error_count + 1;
                }

            } else {
                holder.error.setVisibility(View.GONE);
            }

            final int[] position_temp = {position};

            final double totalPrice_temp = (Double.parseDouble(productsResponseModel.getPrice()) * Double.parseDouble(productsResponseModel.getItem_count()));
            holder.productPrice.setText("Price: Rs " + totalPrice_temp);
            totalPrice = totalPrice + totalPrice_temp;
            if (position == checkoutList.size() - 1) {
                ((CheckoutActivity) context).setPrice(totalPrice, error_count);
            }

            holder.cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancelItem(productsResponseModel.getCart_id(), position_temp[0],totalPrice_temp);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelItem(String cart_id, final int position,final double totalPrice_temp ) {
        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<RegisterResponseModel> call = apiService.deleteItemFromCart(Prefs.getString("apiKey", ""), cart_id);
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                customProgressDialog.cancel();
                if (response.body() != null && response.body().getError() != null && response.body().getError().equalsIgnoreCase("false")) {



                    //REMOVING REMOVED PRODUCT FROM THE LIST
                    totalPrice = 0;
                    error_count=0;
                    checkoutList.remove(position);
                    notifyDataSetChanged();
                    if (checkoutList.size() == 0) {
                        Toast.makeText(context, "No items in cart", Toast.LENGTH_SHORT).show();
                        context.startActivity(new Intent(context, MainActivity.class));
                        ((CheckoutActivity) context).finish();
                    }
                } else {
                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return checkoutList.size();
    }

    public String checkNUll(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "N/A";
    }
}
