package com.grihachikitsa.grihachikitsa.Adapters;

/**
 * Created by Nikil on 12/8/2016.
 */
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.grihachikitsa.grihachikitsa.Activites.ProductCompleteDetailsActivity;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProjectCompletDetailsModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProjectsHistoryResponseModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ProjectHistoryAdapter extends RecyclerView.Adapter<ProjectHistoryAdapter.MyViewHolder> {

    private List<ProjectsHistoryResponseModel> projectsHistoryResponseModels;
    private Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView name,age,weight,gender,service,timePeriod,address,startDate,duration;
        public Button viewDetails;
        public MyViewHolder(View view) {
            super(view);
            //careType = (TextView) view.findViewById(R.id.careType);
            name = (TextView) view.findViewById(R.id.age);
            age = (TextView) view.findViewById(R.id.age);
            weight = (TextView) view.findViewById(R.id.weight);
            service = (TextView) view.findViewById(R.id.serivces);
            timePeriod = (TextView) view.findViewById(R.id.time);
            address = (TextView) view.findViewById(R.id.address);
            startDate = (TextView) view.findViewById(R.id.startdate);
            duration = (TextView) view.findViewById(R.id.end_day);
            gender = (TextView) view.findViewById(R.id.gender);
            viewDetails = (Button) view.findViewById(R.id.viewDetails);

        }
    }


    public ProjectHistoryAdapter(List<ProjectsHistoryResponseModel> projectsHistoryResponseModels,Context context) {
        this.projectsHistoryResponseModels = projectsHistoryResponseModels;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_projectshistory, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ProjectsHistoryResponseModel projectsHistoryResponseModel = projectsHistoryResponseModels.get(position);
        //  holder.title.setText("You took a ride on "+movie.getCreated_at());

        holder.name.setText(checkNull(projectsHistoryResponseModel.getFirst_name())+" "+checkNull(projectsHistoryResponseModel.getLast_name()));
        holder.address.setText("Address: "+checkNullNA(projectsHistoryResponseModel.getAddress()));
        holder.timePeriod.setText("Time Period: "+checkNullNA(projectsHistoryResponseModel.getHours()));
        holder.service.setText("Services: "+checkNullNA(projectsHistoryResponseModel.getService()));
        holder.age.setText("Age: "+checkNullNA(projectsHistoryResponseModel.getAge()));
        holder.weight.setText("Weight: "+checkNullNA(projectsHistoryResponseModel.getWeight()));
        holder.gender.setText("Gender: "+checkNullNA(projectsHistoryResponseModel.getGender()));
        holder.startDate.setText("Start Date: "+checkNullNA(converDateTime(projectsHistoryResponseModel.getCoated_on())));
        holder.duration.setText("Duration: "+checkNullNA(projectsHistoryResponseModel.getDuration()));
        holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ProductCompleteDetailsActivity.class).putExtra("project_id",projectsHistoryResponseModel.getProject_id()).putExtra("completed",true));
            }
        });

    }
    public String checkNull(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "";
    }

    public String checkNullNA(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "NA";
    }

    @Override
    public int getItemCount() {
        return projectsHistoryResponseModels.size();
    }

    public void updateRefernceStatus()
    {

    }

    public String converDateTime(String dateTime) {
        try {


            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = f.parse(dateTime);
            String day = (String) android.text.format.DateFormat.format("dd", date);
            String stringMonth = (String) android.text.format.DateFormat.format("MMM", date);
            String hours = (String) android.text.format.DateFormat.format("HH", date);
            String minutes = (String) android.text.format.DateFormat.format("mm", date);
            Log.d("time", date + "");
            return day + " " + stringMonth + ", " + hours + ":" + minutes;
        } catch (Exception e) {
            return "";
        }
    }
}
