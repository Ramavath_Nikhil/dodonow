package com.grihachikitsa.grihachikitsa.Firebase;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;
import com.grihachikitsa.grihachikitsa.Models.GetQuotesModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.LoginResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.RegisterResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Config;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
        Prefs.putString("regId", token);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


            Call<RegisterResponseModel> call = apiService.updateUserFCMID(Prefs.getString("apiKey",""),token);
            call.enqueue(new Callback<RegisterResponseModel>() {
                @Override
                public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                    if(response.body()!=null)
                    {
                        if(response.body().getError()!=null && !response.body().getError().isEmpty())
                            Log.d("fcm_update","updated");
                        else
                            Log.d("fcm_update","not updated");
                    }
                    else
                    {
                        Log.d("fcm_update","response body null");
                    }
                }

                @Override
                public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

                }
            });



    }

    private void storeRegIdInPref(String token) {
        Prefs.putString("c", token);
    }
}