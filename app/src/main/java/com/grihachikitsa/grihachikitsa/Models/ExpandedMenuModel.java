package com.grihachikitsa.grihachikitsa.Models;

import android.graphics.drawable.Drawable;

/**
 * Created by Nikil on 12/9/2016.
 */
public class ExpandedMenuModel {



    private String iconName;

    public Drawable getIconImg() {
        return iconImg;
    }

    public void setIconImg(Drawable iconImg) {
        this.iconImg = iconImg;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    private Drawable iconImg;

    public String getIconName() {
        return iconName;
    }
}
