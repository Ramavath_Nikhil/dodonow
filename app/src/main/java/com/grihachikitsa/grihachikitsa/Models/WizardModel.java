package com.grihachikitsa.grihachikitsa.Models;

/**
 * Created by Nikil on 9/20/2016.
 */

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.grihachikitsa.grihachikitsa.Fragments.CustomerInfoPage;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;
import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.AbstractWizardModel;
import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.BranchPage;
import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.MultipleFixedChoicePage;
import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.Page;
import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.PageList;
import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.SingleFixedChoicePage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class WizardModel extends AbstractWizardModel {
    public WizardModel(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {

        List<String> services = Arrays.asList(Prefs.getString("services","").split("-"));

        return new PageList(new BranchPage(this, "Gender").addBranch(
                "Male",
                new SingleFixedChoicePage(this, "AGE OF PATIENT").setChoices("40-50",
                        "50-60", "60-70", "70-80", "ABOVE 80")
                        .setRequired(true),

                new SingleFixedChoicePage(this, "HEIGHT OF PATIENT").setChoices(
                        "4.5 - 5.0 FEET", "5.0 - 5.5 FEET", "5.5 - 6.0 FEET", "ABOVE 6 FEET"
                ).setRequired(true), new SingleFixedChoicePage(this, "WEIGHT OF PATIENT").setChoices(
                        "BELOW 55 kgs", "55-64 kgs", "65-74 kgs", "75-84 kgs", "ABOVE 85 kgs"
                ).setRequired(true),



                new MultipleFixedChoicePage(this, "WHAT DO YOU NEED ELDER CARE FOR?").setChoices( services).setRequired(true),


                new SingleFixedChoicePage(this, "NUMBER OF HOURS").setChoices(
                      "10 HOURS/DAY", "24 HOURS").setRequired(true),

                new SingleFixedChoicePage(this, "DURATION OF SERVICE").setChoices("LESS THAN 10 DAYS", "11-20 DAYS", "LESS THAN 30 DAYS", "FEW MONTHS").setRequired(true),
                new CustomerInfoPage(this, "").setRequired(true)
        )

                .addBranch(
                        "FEMALE",
                        new SingleFixedChoicePage(this, "AGE OF PATIENT").setChoices("40-50",
                                "50-60", "60-70", "70-80", "ABOVE 80")
                                .setRequired(true),

                        new SingleFixedChoicePage(this, "HEIGHT OF PATIENT").setChoices(
                                "4.5 - 5.0 FEET", "5.0 - 5.5 FEET", "5.5 - 6.0 FEET", "ABOVE 6 FEET"
                        ).setRequired(true), new SingleFixedChoicePage(this, "WEIGHT OF PATIENT").setChoices(
                                "BELOW 55 kgs", "55-64 kgs", "65-74 kgs", "75-84 kgs", "ABOVE 85 kgs"
                        ).setRequired(true),

                        new MultipleFixedChoicePage(this, "WHAT DO YOU NEED ELDER CARE FOR?").setChoices(services).setRequired(true),


                        new SingleFixedChoicePage(this, "NUMBER OF HOURS").setChoices(
                                "10 HOURS/DAY", "24 HOURS").setRequired(true),

                        new SingleFixedChoicePage(this, "DURATION OF SERVICE").setChoices("LESS THAN 10 DAYS", "11-20 DAYS", "LESS THAN 30 DAYS", "FEW MONTHS").setRequired(true)
                        ,

                        new CustomerInfoPage(this, "").setRequired(true)).setRequired(true));
    }



}