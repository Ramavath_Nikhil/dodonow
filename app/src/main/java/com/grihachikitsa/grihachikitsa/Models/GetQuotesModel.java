package com.grihachikitsa.grihachikitsa.Models;

import java.util.List;

/**
 * Created by Nikil on 12/24/2016.
 */
public class GetQuotesModel {



    private String error;
    private String project_id;
    private String id;
    private String caretaker_id;
    private String coated_price;
    private String organisation_name;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    private String created_at;
    private List<GetQuotesModel> tasks;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaretaker_id() {
        return caretaker_id;
    }

    public void setCaretaker_id(String caretaker_id) {
        this.caretaker_id = caretaker_id;
    }

    public String getCoated_price() {
        return coated_price;
    }

    public void setCoated_price(String coated_price) {
        this.coated_price = coated_price;
    }

    public String getOrganisation_name() {
        return organisation_name;
    }

    public void setOrganisation_name(String organisation_name) {
        this.organisation_name = organisation_name;
    }

    public List<GetQuotesModel> getTasks() {
        return tasks;
    }

    public void setTasks(List<GetQuotesModel> tasks) {
        this.tasks = tasks;
    }
}
