package com.grihachikitsa.grihachikitsa.Models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Nikil on 12/6/2016.
 */
public class OngoingProjectsModel implements Serializable {



    private String project_type;
    private String project_id;
    private String posted_date;
    private String count,error;

    public List<OngoingProjectsModel> getTasks() {
        return tasks;
    }

    public void setTasks(List<OngoingProjectsModel> tasks) {
        this.tasks = tasks;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private List<OngoingProjectsModel> tasks;

    public String getCaretaker_name() {
        return caretaker_name;
    }

    public void setCaretaker_name(String caretaker_name) {
        this.caretaker_name = caretaker_name;
    }

    public String getProject_type() {
        return project_type;
    }

    public void setProject_type(String project_type) {
        this.project_type = project_type;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getPosted_date() {
        return posted_date;
    }

    public void setPosted_date(String posted_date) {
        this.posted_date = posted_date;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    private String caretaker_name;
}
