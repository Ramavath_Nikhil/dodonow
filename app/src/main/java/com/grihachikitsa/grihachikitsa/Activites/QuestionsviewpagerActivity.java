package com.grihachikitsa.grihachikitsa.Activites;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.grihachikitsa.grihachikitsa.Fragments.QuestionsFragment;
import com.grihachikitsa.grihachikitsa.R;


public class QuestionsviewpagerActivity extends ActionBarActivity implements ActionBar.TabListener {
    ViewPager viewPager;
    public android.support.v7.app.ActionBar actionBar;
    public MySwipeAdapter mySwipeAdapter;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_questionsviewpager);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Log.e("Alert", "Lets See if it Works !!!");
                paramThrowable.printStackTrace();
            }
        });

        actionBar = getSupportActionBar();
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        viewPager = (ViewPager) findViewById(R.id.pagerSwipe);
        mySwipeAdapter = new MySwipeAdapter(getSupportFragmentManager());

        viewPager.setAdapter(mySwipeAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        //  tabLayout.addTab(tabLayout.newTab().setText("Free\nTrial"));
        tabLayout.addTab(tabLayout.newTab().setText("Q1"));
        // tabLayout.addTab(tabLayout.newTab().setText("Q2"));
        // tabLayout.addTab(tabLayout.newTab().setText("Q3"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


      /*  ActionBar.Tab tab1 = actionBar.newTab();
        tab1.setText("Que 1");
        tab1.setTabListener(this);

        ActionBar.Tab tab2 = actionBar.newTab();
        tab2.setText("Que 2");
        tab2.setTabListener(this);

        ActionBar.Tab tab3 = actionBar.newTab();
        tab3.setText("Que 3");
        tab3.setTabListener(this);

        actionBar.addTab(tab1);
        actionBar.addTab(tab2);
        actionBar.addTab(tab3);*/

    }


    public void addTabs(View v) {

        final int tabCount = tabLayout.getTabCount() + 1;
        if (tabCount < 11) {
            final String text = "Q " + tabCount;
            TabLayout.Tab tabs = tabLayout.newTab();
            tabs.setText(text);
            // tabs.setTabListener(this);
            //actionBar.addTab(tabs);
            tabLayout.addTab(tabs);
            viewPager.setCurrentItem(mySwipeAdapter.incrementCount());
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
            //For some reason, setting minWidth in xml and then accessing it here doesn't work, returns 0
            int minWidth = 80;
            tabLayout.setMinimumWidth(minWidth);

            //If there are less tabs than needed to necessitate scrolling, set to fixed/fill
            if(tabLayout.getTabCount() < dpWidth/minWidth){
                tabLayout.setTabMode(TabLayout.MODE_FIXED);
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            }else{
                //Otherwise, set to scrollable
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

            }

        }
    }

    public void removeTabs(View v) {
        Log.d("tabCount", "" + actionBar.getTabCount());
        if (actionBar.getTabCount() > 3) {
            actionBar.removeTabAt(actionBar.getTabCount() - 1);
            mySwipeAdapter.decrementCount();
        }
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

}

class MySwipeAdapter extends FragmentStatePagerAdapter {

    private int count = 1;

    public MySwipeAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        QuestionsFragment myFragment = null;
        if (position < 11) {
            myFragment = QuestionsFragment.getInstance(position);
        }
        return myFragment;
    }

    public int incrementCount() {
        count++;
        notifyDataSetChanged();
        return count;
    }

    public int decrementCount() {
        count--;
        notifyDataSetChanged();
        return count;
    }

    @Override
    public int getCount() {
        return count;
    }

}