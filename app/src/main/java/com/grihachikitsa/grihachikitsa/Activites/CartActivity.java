package com.grihachikitsa.grihachikitsa.Activites;

import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.grihachikitsa.grihachikitsa.Adapters.ProductsAdapter;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProductsResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ProductsAdapter mAdapter;

    private List<ProductsResponseModel> productsResponseModels = new ArrayList<>();
    private Button checkOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        }

        elementsInilization();
        getProducts();
        onClickListeners();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(2, intent);
        finish();
    }

    public void checkCartCount() {
        if (productsResponseModels.size() <= 0) {
            onBackPressed();
        }
    }

    public void elementsInilization() {
        //RECYCLERVIEW INTILIAZTION
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CartActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new ProductsAdapter(productsResponseModels, CartActivity.this);
        recyclerView.setAdapter(mAdapter);


        //PROGRESS BAR INITLIZATION
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);

        //BUTTON INTILIZATION
        checkOut = (Button) findViewById(R.id.checkout);
    }

    public void onClickListeners() {
        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                startActivityForResult(new Intent(CartActivity.this, CheckoutActivity.class).putExtra("latlong", intent.getStringExtra("latlong")), 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            getProducts();
        }
    }

    public void getProducts() {

        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ProductsResponseModel> call = apiService.getCartProducts(Prefs.getString("apiKey", ""),"");
        call.enqueue(new Callback<ProductsResponseModel>() {
            @Override
            public void onResponse(Call<ProductsResponseModel> call, Response<ProductsResponseModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    if (productsResponseModels != null && productsResponseModels.size() > 0) {
                        productsResponseModels.clear();
                        mAdapter.notifyDataSetChanged();
                    }


                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                        progressBar.setVisibility(View.GONE);
                        productsResponseModels.addAll(response.body().getTasks());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(CartActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }

                } else {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(CartActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<ProductsResponseModel> call, Throwable t) {

                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                Toast.makeText(CartActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }


        });
    }

}
