package com.grihachikitsa.grihachikitsa.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.grihachikitsa.grihachikitsa.Adapters.OrderHistoryAdapter;
import com.grihachikitsa.grihachikitsa.Adapters.QuotesAdapter;
import com.grihachikitsa.grihachikitsa.Models.GetQuotesModel;
import com.grihachikitsa.grihachikitsa.Models.OngoingProjectsModel;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.OrdersHistoryModel;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersHistoryActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private OrderHistoryAdapter mAdapter;
    private List<OrdersHistoryModel> getQuotesModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        }

        elementsInilization();
        getOrders();


    }

    public void elementsInilization() {
        //RECYCLERVIEW INTILIAZTION
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OrdersHistoryActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new OrderHistoryAdapter(getQuotesModelList, OrdersHistoryActivity.this);
        recyclerView.setAdapter(mAdapter);


        //PROGRESS BAR INITLIZATION
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
    }


    public void getOrders() {


                progressBar.setVisibility(View.VISIBLE);
                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);


                Call<OrdersHistoryModel> call = apiService.getUserOrders(Prefs.getString("apiKey", ""));
                call.enqueue(new Callback<OrdersHistoryModel>() {
                    @Override
                    public void onResponse(Call<OrdersHistoryModel> call, Response<OrdersHistoryModel> response) {

                        if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                            if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                                progressBar.setVisibility(View.GONE);
                                getQuotesModelList.addAll(response.body().getTasks());
                                mAdapter.notifyDataSetChanged();
                            } else {
                                progressBar.setVisibility(View.GONE);
                            }

                        } else {
                            progressBar.setVisibility(View.GONE);

                            Log.d("response", "null response");
                        }
                    }

                    @Override
                    public void onFailure(Call<OrdersHistoryModel> call, Throwable t) {

                        t.printStackTrace();
                        progressBar.setVisibility(View.GONE);

                    }


                });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
