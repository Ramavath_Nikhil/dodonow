package com.grihachikitsa.grihachikitsa.Activites;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.internal.player.ProfileSettingsEntityCreator;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.grihachikitsa.grihachikitsa.Adapters.ExpandableListAdapter;
import com.grihachikitsa.grihachikitsa.Firebase.NotificationUtils;
import com.grihachikitsa.grihachikitsa.Models.ExpandedMenuModel;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.LoginResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.OrdersHistoryModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.RegisterResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Config;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private RelativeLayout elderCareLayout, healthCheckUpLayout;
    private FloatingActionButton searchLocation, getLocation;
    private GoogleMap googleMap;
    private static final String TAG = MainActivity.class.getSimpleName();
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters
    private static final int REQUEST_LOCATION = 526;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar toolbar;
    ExpandableListAdapter mMenuAdapter;
    ExpandableListView expandableList;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private TextView headerName, headerEmail;
    private CircleImageView profilePic;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private static final int CALL_REQUEST = 501;
private EditText address;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        elementsInitilization();
        onClickListeners();
        printingNavigationHeaderData();
        registerNotificationReceiver();
    }

    @Override
    public void onBackPressed() {

        finish();
    }

    public void mapLastProjectLocation() {
        LoginResponseModel loginResponseModel = new Gson().fromJson(Prefs.getString("user_details", ""), LoginResponseModel.class);
        if (loginResponseModel != null && loginResponseModel.getLast_project_location() != null && !loginResponseModel.getLast_project_location().isEmpty()) {
            String[] latLongPoints = loginResponseModel.getLast_project_location().split(",");
            if (latLongPoints != null && latLongPoints.length == 2) {
                LatLng latLng = new LatLng(Double.parseDouble(latLongPoints[0]), Double.parseDouble(latLongPoints[1]));
                MarkerOptions marker = new MarkerOptions().position(latLng).title("Your last project location");
                googleMap.addMarker(marker);

                // animate google map
                CameraPosition cameraPosition = new CameraPosition.Builder().target(
                        latLng).zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
    }

    public void elementsInitilization() {


        //EDITTEXT INTILIZATION
        address = (EditText) findViewById(R.id.address);


        //RELATIVE LAYOUT INTILIZATION
        elderCareLayout = (RelativeLayout) findViewById(R.id.eldercarelayout);
        healthCheckUpLayout = (RelativeLayout) findViewById(R.id.healthcheckuplayout);


        //FLOATING ACTION BUTTON INTILIZATION
        searchLocation = (FloatingActionButton) findViewById(R.id.searchlocation);
        searchLocation.setClickable(false);
        getLocation = (FloatingActionButton) findViewById(R.id.getLocation);
        getLocation.setClickable(false);

        // MAP INTILILZATION
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        //  EXPANDABLE LIST VIEW INTILIZATION
        expandableList = (ExpandableListView) findViewById(R.id.navigationmenu);
        expandableList.setGroupIndicator(null);
        prepareListData();
        mMenuAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild, expandableList);

        // setting list adapter
        expandableList.setAdapter(mMenuAdapter);


        //NAVIGATION DRAWER INTILIZAITON
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                printingNavigationHeaderData();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();

                if (menuItem.getItemId() == R.id.projects) {
                    startActivity(new Intent(MainActivity.this, ProjectsActivity.class));
                }


                return false;
            }

        });


    }


    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        // Adding data header
        listDataHeader.add("My account");
        listDataHeader.add("Projects");
        listDataHeader.add("About GC");
        listDataHeader.add("Logout");

        // Adding child data
        List<String> heading1 = new ArrayList<String>();
        heading1.add("Edit profile");
        heading1.add("Orders");
        heading1.add("Support");


        listDataChild.put(listDataHeader.get(0), heading1);// Header, Child data
        //  listDataChild.put(listDataHeader.get(1), heading2);

    }

    public void onClickListeners() {

        //RELATIVE LAYOUT ONCLICK LISTENER
        elderCareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Gson gson = new Gson();
                LoginResponseModel loginResponseModel = gson.fromJson(Prefs.getString("user_details", ""), LoginResponseModel.class);


                if (loginResponseModel.getStatus().equals("2")) {
                    /*if (googleMap.getCameraPosition().target.latitude == 0 || googleMap.getCameraPosition().target.longitude == 0) {
                       Toast.makeText(MainActivity.this, "Please provide your location", Toast.LENGTH_SHORT).show();
                    } else {
                        createProject();
                    }*/

                    if(address.getText().toString().isEmpty())
                    {
                        Toast.makeText(MainActivity.this, "Please provide your location", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        createProject();
                    }


                } else {
                    Toast.makeText(MainActivity.this, "Your account is in disable mode, please request admin to activate", Toast.LENGTH_SHORT).show();
                }

            }
        });

        healthCheckUpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (googleMap.getCameraPosition().target.latitude == 0 || googleMap.getCameraPosition().target.longitude == 0) {
                    Toast.makeText(MainActivity.this, "Please provide your location", Toast.LENGTH_SHORT).show();
                } else {
                    buyOrRentOption();
                }

            }
        });

        //FLAOTING ACTION BUTTON ONCLICK LISTENERS
        searchLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent destIntent =
                            new PlaceAutocomplete
                                    .IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .build(MainActivity.this);
                    startActivityForResult(destIntent, 1);
                } catch (GooglePlayServicesRepairableException e) {
                    Toast.makeText(MainActivity.this, "Something went wrong. Try agian!", Toast.LENGTH_LONG).show();
                    Log.d("Repairable", "" + e.getLocalizedMessage());
                } catch (GooglePlayServicesNotAvailableException e) {
                    Toast.makeText(MainActivity.this, "Something went wrong. Try agian!", Toast.LENGTH_LONG).show();
                    Log.d("ServicesNotAvailable", "" + e.getLocalizedMessage());
                }
            }
        });

        getLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // First we need to check availability of play services
                if (checkPlayServices()) {

                    // Building the GoogleApi client
                    buildGoogleApiClient();
                }
            }
        });


        //EXPANDABLE LIST VIEWE ONCLICK LISTENERS
        expandableList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {

                if (i == 1) {
                    startActivity(new Intent(MainActivity.this, ProjectsActivity.class));
                }

                if (i == 3) {
                    logout();
                }
                return false;
            }
        });
        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {

                if (i1 == 0) {
                    startActivity(new Intent(MainActivity.this, ProfileEditActivity.class));
                } else if (i1 == 1) {
                    startActivity(new Intent(MainActivity.this, OrdersHistoryActivity.class));
                } else if (i1 == 2) {
                    startActivity(new Intent(MainActivity.this, ContactUsActivity.class));
                }
                return false;
            }
        });
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

    }


    public void printingNavigationHeaderData() {

        View header = mNavigationView.getHeaderView(0);
        headerName = (TextView) header.findViewById(R.id.name);
        headerEmail = (TextView) header.findViewById(R.id.email);
        profilePic = (CircleImageView) header.findViewById(R.id.profilepic);
        Gson gson = new Gson();
        LoginResponseModel loginResponseModel = gson.fromJson(Prefs.getString("user_details", ""), LoginResponseModel.class);
        if (loginResponseModel != null) {
            if (loginResponseModel.getFirst_name() != null && !loginResponseModel.getFirst_name().isEmpty()) {
                headerName.setText(loginResponseModel.getFirst_name());
            } else {
                headerName.setText("");
            }
            if (loginResponseModel.getLast_name() != null && !loginResponseModel.getLast_name().isEmpty()) {
                headerName.setText(headerName.getText().toString() + " " + loginResponseModel.getLast_name());
            }

            if (loginResponseModel.getEmail() != null && !loginResponseModel.getEmail().isEmpty()) {
                headerEmail.setText(loginResponseModel.getEmail());
            }
            Log.d("imageurl", Config.BASE_URL + loginResponseModel.getProfile_pic());
            Picasso
                    .with(this)
                    .load(Config.BASE_URL + loginResponseModel.getProfile_pic())
                    .placeholder(R.mipmap.ic_account_circle_black_24dp)
                    .error(R.mipmap.ic_gclogo)
                    .into(profilePic);
          /*  Glide.with(this)
                    .load(Config.BASE_URL + loginResponseModel.getProfile_pic())
                    .into(profilePic)*/

            ;
        } else {
            headerName.setVisibility(View.GONE);
            headerEmail.setVisibility(View.GONE);
        }
    }

    public void registerNotificationReceiver() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    sendRegistrationToServer(Prefs.getString("regId", ""));


                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                    //handleNotification(message);


                }
            }
        };
    }


    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();

// register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));


        sendRegistrationToServer(Prefs.getString("regId", ""));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {


            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.showNotificationMessage(getResources().getString(R.string.app_name), message, null, new Intent());
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
        Prefs.putString("regId", token);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<RegisterResponseModel> call = apiService.updateUserFCMID(Prefs.getString("apiKey", ""), token);
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                if (response.body() != null) {
                    if (response.body().getError() != null && !response.body().getError().isEmpty())
                        Log.d("fcm_update", "updated");
                    else
                        Log.d("fcm_update", "not updated");
                } else {
                    Log.d("fcm_update", "response body null");
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

            }
        });


    }


    public void buyOrRentOption() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_rentorbuy);
        Button buy = (Button) dialog.findViewById(R.id.buy);
        Button rent = (Button) dialog.findViewById(R.id.rent);

        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                Intent intent = new Intent(MainActivity.this, ProductListingActivity.class);
                intent.putExtra("latlong", googleMap.getCameraPosition().target.latitude + "," + googleMap.getCameraPosition().target.longitude);
                intent.putExtra("type", "buy");
                startActivity(intent);
            }
        });

        rent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.cancel();
                Intent intent = new Intent(MainActivity.this, ProductListingActivity.class);
                intent.putExtra("latlong", googleMap.getCameraPosition().target.latitude + "," + googleMap.getCameraPosition().target.longitude);
                intent.putExtra("type", "rent");
                startActivity(intent);

            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);
    }

    public void logout() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_logout);
        Button logout = (Button) dialog.findViewById(R.id.logout);
        Button cancel = (Button) dialog.findViewById(R.id.cancel);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                startActivity(new Intent(MainActivity.this, SplashActivity.class));
                Prefs.putString("user_details", "");
                Prefs.putString("apiKey", "");
                finish();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.cancel();


            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_call) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        CALL_REQUEST);
                return false;
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse(Config.ORGANISATION_MOBILE_NUMBER));
                startActivity(callIntent);
            }

        }
        return super.onOptionsItemSelected(item);
    }


    public void createProject() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_patient_name);
        Button next = (Button) dialog.findViewById(R.id.next);
        final EditText patinentName = (EditText) dialog.findViewById(R.id.patientName);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!patinentName.getText().toString().isEmpty()) {
                    dialog.cancel();
                    Intent intent = new Intent(MainActivity.this, QuestionsActivity.class);
                    intent.putExtra("name", patinentName.getText().toString());
                    intent.putExtra("address",address.getText().toString());
                    intent.putExtra("latlong", googleMap.getCameraPosition().target.latitude + "," + googleMap.getCameraPosition().target.longitude);
                    startActivity(intent);

                } else {
                    Toast.makeText(MainActivity.this, "Please enter patient name", Toast.LENGTH_SHORT).show();
                }

            }
        });


        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        displayLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
    }

    /**
     * Method to display the location on UI
     */
    private void displayLocation() {

        boolean hasPermissionFine = (ContextCompat.checkSelfPermission(MainActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        boolean hasPermissionCoarse = (ContextCompat.checkSelfPermission(MainActivity.this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermissionFine || !hasPermissionCoarse) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION);
        } else {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                double latitude = mLastLocation.getLatitude();
                double longitude = mLastLocation.getLongitude();
                mapMarkerSettings(new LatLng(latitude, longitude), "Your Location");

            } else {

                Toast.makeText(MainActivity.this, "Couldn't get the location. Make sure location is enabled on the device", Toast.LENGTH_SHORT).show();

            }
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    if (checkPlayServices()) {

                        // Building the GoogleApi client
                        buildGoogleApiClient();
                    }

                } else {

                    Toast.makeText(MainActivity.this, "Please provide us the required permission to auto fill your address", Toast.LENGTH_SHORT).show();
                }
                break;

            case CALL_REQUEST:


                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse(Config.ORGANISATION_MOBILE_NUMBER));
                    startActivity(callIntent);

                } else {

                    Toast.makeText(MainActivity.this, "Please provide reqiured permission to make a call", Toast.LENGTH_SHORT).show();
                }


                break;


        }

    }

    public void mapMarkerSettings(LatLng latLng, String name) {
        googleMap.clear();
        // create marker
        MarkerOptions marker = new MarkerOptions().position(latLng).title(name + "");
        googleMap.addMarker(marker);

        // animate google map
        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                latLng).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                // retrive the data by using getPlace() method.
                Place place = PlaceAutocomplete.getPlace(this, data);
                mapMarkerSettings(place.getLatLng(), place.getName().toString() + "");

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.e("Tag", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        getLocation.setClickable(true);
        searchLocation.setClickable(true);
        mapLastProjectLocation();
    }
}
