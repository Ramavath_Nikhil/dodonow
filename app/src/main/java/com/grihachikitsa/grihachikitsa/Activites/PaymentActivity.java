package com.grihachikitsa.grihachikitsa.Activites;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.AddToCartResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.CouponResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.CreateOrderResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;
import com.grihachikitsa.grihachikitsa.Views.CustomProgressDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends AppCompatActivity {





    private Button confirmOrder,applyCouponButton;
    private RadioGroup radioGroup;
    private double totalPrice,subtotal;
    private TextView subTotal,finalAmount,couponAmountextview,couponAmount;
    private String address,latlong;
    private EditText couponEditText;
    private CouponResponseModel couponResponseModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        }
        elementsIntilizataion();
        onClickLiseners();

    }


    public void elementsIntilizataion()
    {
        //BUTTON INTILIZATION
        confirmOrder = (Button) findViewById(R.id.confirm);
        applyCouponButton = (Button) findViewById(R.id.couponApply);

        //RADIO GROUP INTILIZATION
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        //TEXTVIEW INTILIZATION
        subTotal = (TextView) findViewById(R.id.subtotal);
        finalAmount = (TextView) findViewById(R.id.finalAmount);
        couponAmount = (TextView) findViewById(R.id.couponamount);
        couponAmountextview = (TextView) findViewById(R.id.couponamountextview);

        //EDITEXTINTILIZATION
        couponEditText = (EditText) findViewById(R.id.coupon);

        Intent intent = getIntent();
        if(intent!=null)
        {
            subtotal = intent.getDoubleExtra("totalPrice",0);
            totalPrice = subtotal;
            subTotal.setText("Rs "+totalPrice);
            finalAmount.setText("Rs "+totalPrice);
            address = intent.getStringExtra("address");
            latlong = intent.getStringExtra("latlong");
        }

    }


    public  void applyCoupon()
    {
        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(PaymentActivity.this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<CouponResponseModel> call = apiService.applycoupon(Prefs.getString("apiKey", ""), subtotal+"",couponEditText.getText().toString());
        call.enqueue(new Callback<CouponResponseModel>() {
            @Override
            public void onResponse(Call<CouponResponseModel> call, Response<CouponResponseModel> response) {
                customProgressDialog.cancel();
                if(response.body()!=null && response.body().getError().equalsIgnoreCase("false"))
                {
                    couponResponseModel = response.body();
                   if(couponResponseModel!=null) {
                       totalPrice = Double.parseDouble(couponResponseModel.getTotalprice());
                       finalAmount.setText("Rs " + totalPrice);
                       couponAmount.setVisibility(View.VISIBLE);
                       couponAmountextview.setVisibility(View.VISIBLE);
                       couponAmount.setText("- " + couponResponseModel.getDiscount_price());
                       Toast.makeText(PaymentActivity.this, "Discount of Rs " + couponResponseModel.getDiscount_price() + " applied", Toast.LENGTH_SHORT).show();
                   }
                    else
                   {
                       couponAmount.setVisibility(View.GONE);
                       couponAmountextview.setVisibility(View.GONE);
                       finalAmount.setText("Rs "+subtotal);
                       if(response.body()!=null && response.body().getMessage()!=null && !response.body().getMessage().isEmpty())
                       {
                           Toast.makeText(PaymentActivity.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                       }
                       else {
                           Toast.makeText(PaymentActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
                       }

                   }
                }
                else
                {
                    couponAmount.setVisibility(View.GONE);
                    couponAmountextview.setVisibility(View.GONE);
                    finalAmount.setText("Rs "+subtotal);
                    if(response.body()!=null && response.body().getMessage()!=null && !response.body().getMessage().isEmpty())
                    {
                        Toast.makeText(PaymentActivity.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(PaymentActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<CouponResponseModel> call, Throwable t) {

                customProgressDialog.cancel();
                couponAmount.setVisibility(View.GONE);
                couponAmountextview.setVisibility(View.GONE);
                Toast.makeText(PaymentActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onClickLiseners()
    {

        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId=radioGroup.getCheckedRadioButtonId();

                if(selectedId ==R.id.onlinePayment)
                {
                    confirmOrer("1");
                }
                else
                {
                    confirmOrer("0");
                }

            }
        });

        applyCouponButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(couponEditText.getText().toString().isEmpty())
                {
                    Toast.makeText(PaymentActivity.this,"Please provide a valid coupon",Toast.LENGTH_SHORT).show();
                }
                else {
                    applyCoupon();
                }
            }
        });


    }

    public void confirmOrer(String paymentType)
    {
        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(PaymentActivity.this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<CreateOrderResponseModel> call = apiService.createOrder(Prefs.getString("apiKey", ""), paymentType,address,latlong);
        call.enqueue(new Callback<CreateOrderResponseModel>() {
            @Override
            public void onResponse(Call<CreateOrderResponseModel> call, Response<CreateOrderResponseModel> response) {

                customProgressDialog.cancel();
                if(response.body()!=null && response.body().getError()!=null && response.body().getError().equalsIgnoreCase("false"))
                {
                    showMessageDialog("Your order has been placed succesfully",new Intent(PaymentActivity.this,MainActivity.class),PaymentActivity.this);
                }
                else
                {
                    Toast.makeText(PaymentActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CreateOrderResponseModel> call, Throwable t) {
                customProgressDialog.cancel();
                Toast.makeText(PaymentActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void showMessageDialog(String message, final Intent intent, Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_message);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Button ok = (Button) dialog.findViewById(R.id.ok);
        TextView messageTextview = (TextView) dialog.findViewById(R.id.message);
        messageTextview.setText(message);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (intent != null) {
                    startActivity(intent);
                } else {
                    onBackPressed();
                }
            }
        });


        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);
    }


}
