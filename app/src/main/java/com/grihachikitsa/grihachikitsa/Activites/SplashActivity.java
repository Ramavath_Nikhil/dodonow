package com.grihachikitsa.grihachikitsa.Activites;

import android.content.ContextWrapper;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.grihachikitsa.grihachikitsa.Models.OngoingProjectsModel;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;

import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.LoginResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;
import com.grihachikitsa.grihachikitsa.Views.CustomProgressDialog;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private Button login, register;
    private RelativeLayout loginLayout;
    private TextView loginRegister;
    private EditText loginEmail, loginPassword;
    private Boolean loginBoolean = false;
    private LinearLayout bottomLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        elementsInitilization();
        onClickListeners();
    }


    public void elementsInitilization() {

        //BUTTONS INTILIZATION
        login = (Button) findViewById(R.id.login);
        register = (Button) findViewById(R.id.register);

        //RELATIVE LAYOUTS INTILIZATION
        loginLayout = (RelativeLayout) findViewById(R.id.loginlayout);

        //TEXTVIEWS INITILIZATION
        loginRegister = (TextView) findViewById(R.id.loginRegister);

        //EDITTEXT INTITILIZATION
        loginEmail = (EditText) findViewById(R.id.loginEmail);
        loginPassword = (EditText) findViewById(R.id.loginPassword);

        //LINEAR LAYOUT INTLIZATOIN
        bottomLayout = (LinearLayout) findViewById(R.id.belowlayout);
    }

    public void onClickListeners() {


        //BUTTONS ON CLICK LISTENERS
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (loginBoolean) {
                    if (loginEmail.getText().toString().isEmpty() || !isValidEmail(loginEmail.getText().toString())) {
                        Toast.makeText(SplashActivity.this, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
                    } else {
                        if (loginPassword.getText().toString().isEmpty()) {
                            Toast.makeText(SplashActivity.this, "Please enter your password", Toast.LENGTH_SHORT).show();
                        } else {
                            // startActivity(new Intent(SplashActivity.this, MainActivity.class));
                            login();
                        }
                    }
                } else {
                    loginBoolean = true;
                    loginLayout.setVisibility(View.VISIBLE);
                    register.setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            0, LinearLayout.LayoutParams.MATCH_PARENT);
                    params.weight = 2.0f;
                    login.setLayoutParams(params);
                }

            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SplashActivity.this, RegisterActivity.class));
            }
        });


        // TEXTVIEW ONCLICK LISTENERS

        loginRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SplashActivity.this, RegisterActivity.class));
            }
        });


    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public void login() {

        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(SplashActivity.this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<LoginResponseModel> call = apiService.login(loginEmail.getText().toString(), loginPassword.getText().toString());
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {
                    Log.d("Response", response.body().toString() + "");
                    customProgressDialog.cancel();
                    Gson gson = new Gson();
                    Prefs.putString("user_details", gson.toJson(response.body()));
                    Prefs.putString("apiKey",response.body().getApiKey());
                    startActivity(new Intent(SplashActivity.this,MainActivity.class));
                    finish();

                } else {
                    customProgressDialog.cancel();
                    if(response.body()!=null && response.body().getError().equalsIgnoreCase("true"))
                    {
                        if(response.body().getMessage()!=null && !response.body().getMessage().isEmpty())
                        {
                            Toast.makeText(SplashActivity.this,response.body().getMessage() ,Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(SplashActivity.this,"something went wrong",Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                    {
                        Toast.makeText(SplashActivity.this,"something went wrong",Toast.LENGTH_SHORT).show();
                        Log.d("response", "null response");
                    }



                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {

                t.printStackTrace();
                customProgressDialog.cancel();
                Toast.makeText(SplashActivity.this,"something went wrong",Toast.LENGTH_SHORT).show();
            }


        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!Prefs.getString("apiKey","").isEmpty())
        {
           getUserDetailsByEmail();
            bottomLayout.setVisibility(View.GONE);
        }
        else
        {
            bottomLayout.setVisibility(View.VISIBLE);
            Toast.makeText(SplashActivity.this, "Please login to continue", Toast.LENGTH_SHORT).show();
        }
    }

    public void getUserDetailsByEmail()
    {

        Gson gson = new Gson();
        LoginResponseModel loginResponseModel = gson.fromJson(Prefs.getString("user_details",""),LoginResponseModel.class);

        if(loginResponseModel!=null) {
            final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(SplashActivity.this);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);


            Call<LoginResponseModel> call = apiService.fetchUserDetailsbyMail(loginResponseModel.getEmail());
            call.enqueue(new Callback<LoginResponseModel>() {
                @Override
                public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {


                    if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {
                        Log.d("Response", response.body().toString() + "");
                        customProgressDialog.cancel();
                        Gson gson = new Gson();
                       /* ArrayList<String > services = new ArrayList<String>();
                        services.add("Oral medication, Oxygen and stoma care");
                        services.add("Assistance in daily living ( feeding, bathing, movement and general hygiene )");
                        services.add("Lactation support");
                        services.add("Post surgical care");
                        services.add("Urine catheterization");
                        services.add("Wound care");
                        services.add("Oxygen administration");
                        services.add("Tracheostomy");
                        services.add("Suture removal");
                        services.add("Tube feeding");
                        services.add("Injections ( IV, IM, SC )");
                        services.add("Vitals monitoring");*/

                        Prefs.putString("user_details", gson.toJson(response.body()));
                        Prefs.putString("apiKey", response.body().getApiKey());
                        Prefs.putString("services", response.body().getServices());
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();


                    } else {
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));

                        customProgressDialog.cancel();
                        Log.d("response", "null response");
                        finish();
                    }


                }

                @Override
                public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    t.printStackTrace();
                    customProgressDialog.cancel();
                    finish();
                }


            });
        }
        else
        {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
        }
    }

}
