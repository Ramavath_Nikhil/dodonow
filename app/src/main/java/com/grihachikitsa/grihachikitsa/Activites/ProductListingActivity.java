package com.grihachikitsa.grihachikitsa.Activites;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.grihachikitsa.grihachikitsa.Adapters.ProductsAdapter;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.CartCountResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProductsResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;
import com.grihachikitsa.grihachikitsa.Views.BadgeDrawable;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;

public class ProductListingActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ProductsAdapter mAdapter;
    private LayerDrawable mCartMenuIcon;
    private int mCartCount;
    private List<ProductsResponseModel> productsResponseModels = new ArrayList<>();
    private ErrorView errorView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_listing);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        }



        elementsInilization();
        getCartCount();

    }

    public void getCartCount() {

        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<CartCountResponseModel> call = apiService.getUserCartCount(Prefs.getString("apiKey", ""));
        call.enqueue(new Callback<CartCountResponseModel>() {
            @Override
            public void onResponse(Call<CartCountResponseModel> call, Response<CartCountResponseModel> response) {
                if (response != null && response.body() != null && response.body().getError().equalsIgnoreCase("false")) {



                    if (response.body().getCart_count() != null && !response.body().getCart_count().isEmpty()) {
                        mCartCount = Integer.parseInt(response.body().getCart_count());
                        setBadgeCount(ProductListingActivity.this, mCartMenuIcon, String.valueOf(mCartCount));

                    } else {
                        mCartCount = 0;
                        setBadgeCount(ProductListingActivity.this, mCartMenuIcon, String.valueOf(mCartCount));
                    }

                    Intent intent = getIntent();
                    if(intent.hasExtra("type"))
                    {
                        if(intent.getStringExtra("type").equalsIgnoreCase("rent"))
                        {
                            getProducts("rent");
                        }
                        else
                        {
                            getProducts("buy");
                        }
                    }

                } else {
                    Toast.makeText(ProductListingActivity.this, "Sorry we are currently not available", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CartCountResponseModel> call, Throwable t) {
                Toast.makeText(ProductListingActivity.this, "Somthing went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void elementsInilization() {
        //RECYCLERVIEW INTILIAZTION
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductListingActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new ProductsAdapter(productsResponseModels, ProductListingActivity.this);
        recyclerView.setAdapter(mAdapter);


        //ERRORVIEW INTITLIZATION
        errorView = (ErrorView) findViewById(R.id.error_view);
        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                errorView.setVisibility(View.GONE);
                Intent intent = getIntent();
                if(intent.hasExtra("type"))
                {
                    if(intent.getStringExtra("type").equalsIgnoreCase("rent"))
                    {
                        getProducts("rent");
                    }
                    else
                    {
                        getProducts("buy");
                    }
                }

            }
        });

        //PROGRESS BAR INITLIZATION
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
    }


    public void getProducts(String type) {

        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ProductsResponseModel> call = apiService.getRentProducts(Prefs.getString("apiKey", ""),type);
        call.enqueue(new Callback<ProductsResponseModel>() {
            @Override
            public void onResponse(Call<ProductsResponseModel> call, Response<ProductsResponseModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    if (productsResponseModels != null && productsResponseModels.size() > 0) {
                        productsResponseModels.clear();
                        mAdapter.notifyDataSetChanged();

                    }
                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        productsResponseModels.addAll(response.body().getTasks());

                        errorView.setVisibility(View.GONE);
                    } else {
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                        errorView.setVisibility(View.VISIBLE);
                        errorView.setTitle("No products available");
                        errorView.setSubtitle("Please retry after some time");
                    }

                } else {

                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                    errorView.setVisibility(View.VISIBLE);
                    errorView.setTitle("No products available");
                    errorView.setSubtitle("Please retry after some time");
                }
            }

            @Override
            public void onFailure(Call<ProductsResponseModel> call, Throwable t) {

                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                errorView.setTitle("Something went wrong");
                errorView.setSubtitle("Please retry after some time");

            }


        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_products, menu);
        mCartMenuIcon = (LayerDrawable) menu.findItem(R.id.action_cart).getIcon();
        setBadgeCount(ProductListingActivity.this, mCartMenuIcon, String.valueOf(mCartCount));



        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:

                if (mCartCount > 0)
                {
                    Intent intent = getIntent();
                    startActivityForResult(new Intent(ProductListingActivity.this, CartActivity.class).putExtra("latlong",intent.getStringExtra("latlong")), 1);
                }

                else
                    Toast.makeText(ProductListingActivity.this, "please add items to continue", Toast.LENGTH_SHORT).show();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            getCartCount();
        }
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    public void onClickIncrementCartCount() {
        mCartCount = mCartCount + 1;
        setBadgeCount(this, mCartMenuIcon, String.valueOf(mCartCount));
    }

    public void onClickDecrementCartCount() {
        mCartCount = mCartCount - 1;
        setBadgeCount(this, mCartMenuIcon, String.valueOf(mCartCount));
    }

}
