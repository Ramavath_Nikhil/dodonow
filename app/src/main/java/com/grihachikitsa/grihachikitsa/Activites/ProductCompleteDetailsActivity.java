package com.grihachikitsa.grihachikitsa.Activites;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.CareTakerCompleteDetailsModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProductsResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProjectCompletDetailsModel;
import com.grihachikitsa.grihachikitsa.Utils.Config;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;
import com.grihachikitsa.grihachikitsa.Views.CustomProgressDialog;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductCompleteDetailsActivity extends AppCompatActivity {


    public TextView patientName, patientAge, patientHeight, patientWeight, patientGender, patientServices, hours, duration, moreinfo, address;
    private TextView quotes, careTakerName, organisationName, coatedPrice, acceptedOn;
    private Button viewQuotes, careTakerDetails, organisationDetails;
    private LinearLayout careTakerDetailsLayout;
    private TextView careTakerDetailsTitle;
    private RelativeLayout mainLayout;
    private ProjectCompletDetailsModel projectCompletDetailsModel;
    private boolean completed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_complete_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        }

        elementsIntilization();
        onClickListeners();
        getProjectDetails();

    }


    public void elementsIntilization() {
        //TEXTVIEW INTILIZATION
        patientName = (TextView) findViewById(R.id.name);
        patientAge = (TextView) findViewById(R.id.age);
        patientHeight = (TextView) findViewById(R.id.height);
        patientWeight = (TextView) findViewById(R.id.weight);
        patientGender = (TextView) findViewById(R.id.gender);
        patientServices = (TextView) findViewById(R.id.serivces);
        hours = (TextView) findViewById(R.id.hours);
        duration = (TextView) findViewById(R.id.duration);
        moreinfo = (TextView) findViewById(R.id.moreinfo);
        address = (TextView) findViewById(R.id.address);
        quotes = (TextView) findViewById(R.id.quotes);
        careTakerName = (TextView) findViewById(R.id.careTakerName);
        organisationName = (TextView) findViewById(R.id.organisationName);
        coatedPrice = (TextView) findViewById(R.id.coatedPrice);
        acceptedOn = (TextView) findViewById(R.id.coatedOn);
        careTakerDetailsTitle = (TextView) findViewById(R.id.careTakerDetails);

        //BUTTON INTILIZATION
        viewQuotes = (Button) findViewById(R.id.viewQuotes);
        careTakerDetails = (Button) findViewById(R.id.careTakerCompleteDetails);
        organisationDetails = (Button) findViewById(R.id.organisationCompleteDetails);

        //LINEAR LAYOUT INTILIZATION
        careTakerDetailsLayout = (LinearLayout) findViewById(R.id.careTakerDetailsLayout);

        //RELATIVE LAYOUT INTILIZATION
        mainLayout = (RelativeLayout) findViewById(R.id.mainlayout);

    }

    public void onClickListeners() {
        viewQuotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        careTakerDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCareTakerDetails(projectCompletDetailsModel.getCaretaker_id());
            }
        });

        organisationDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void getCareTakerDetails(String id) {
        final CustomProgressDialog progressBar = CustomProgressDialog.show(ProductCompleteDetailsActivity.this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<CareTakerCompleteDetailsModel> call = apiService.getCareTakerCompleteDetails(id);
        call.enqueue(new Callback<CareTakerCompleteDetailsModel>() {
            @Override
            public void onResponse(Call<CareTakerCompleteDetailsModel> call, Response<CareTakerCompleteDetailsModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                        progressBar.cancel();
                        printUserDetails(response.body().getTasks().get(0));
                    } else {
                        progressBar.cancel();
                    }

                } else {
                    progressBar.cancel();

                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<CareTakerCompleteDetailsModel> call, Throwable t) {

                t.printStackTrace();
                progressBar.cancel();

            }


        });


    }

    public void printUserDetails(CareTakerCompleteDetailsModel careTakerCompleteDetailsModel) {
        if (careTakerCompleteDetailsModel != null) {
            TextView first_name, last_name, email, mobile, gender, age, doj, qualification, experience, designation, address, residenceNumber, id_addressProof, bloodGroup, father, mother, spouse, martialStatus, childre, backgroundVerification;
            Button ok, bookcareTaker;

            final Dialog dialog = new Dialog(ProductCompleteDetailsActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.layout_popup_caretakerdetails);
            CircleImageView profilePic = (CircleImageView) dialog.findViewById(R.id.profilepic);
            first_name = (TextView) dialog.findViewById(R.id.firstname);

            email = (TextView) dialog.findViewById(R.id.email);
            mobile = (TextView) dialog.findViewById(R.id.mobile);
            gender = (TextView) dialog.findViewById(R.id.gender);
            age = (TextView) dialog.findViewById(R.id.age);
            doj = (TextView) dialog.findViewById(R.id.doj);
            qualification = (TextView) dialog.findViewById(R.id.qualification);
            experience = (TextView) dialog.findViewById(R.id.experience);
            designation = (TextView) dialog.findViewById(R.id.designation);
            residenceNumber = (TextView) dialog.findViewById(R.id.residncenumber);
            id_addressProof = (TextView) dialog.findViewById(R.id.addressProof);
            bloodGroup = (TextView) dialog.findViewById(R.id.bloodgroup);
            father = (TextView) dialog.findViewById(R.id.father);
            mother = (TextView) dialog.findViewById(R.id.mother);
            martialStatus = (TextView) dialog.findViewById(R.id.martial_status);
            spouse = (TextView) dialog.findViewById(R.id.spouse);
            childre = (TextView) dialog.findViewById(R.id.children);
            backgroundVerification = (TextView) dialog.findViewById(R.id.backgroundVerification);
            address = (TextView) dialog.findViewById(R.id.address);

            //BUTTON ON CLICK LISTENER
            ok = (Button) dialog.findViewById(R.id.ok);
            bookcareTaker = (Button) dialog.findViewById(R.id.bookCareTaker);
            bookcareTaker.setVisibility(View.GONE);

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT, 2.0f);
            ok.setLayoutParams(param);


            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
            //CIRCULAT IMAGE VIEW


            first_name.setText("Name: " + checkNull(careTakerCompleteDetailsModel.getCaretaker_name()));
            email.setText("Email: " + checkNull(careTakerCompleteDetailsModel.getEmail()));
            mobile.setText("Mobile: " + checkNull(careTakerCompleteDetailsModel.getMobile()));
            gender.setText("Gender: " + checkNull(careTakerCompleteDetailsModel.getGender()));
            age.setText("Age: " + checkNull(careTakerCompleteDetailsModel.getAge()));
            doj.setText("Date of joinging: " + checkNull(careTakerCompleteDetailsModel.getDOJ()));
            qualification.setText("Qualification: " + checkNull(careTakerCompleteDetailsModel.getQualification()));
            experience.setText("Experience: " + checkNull(careTakerCompleteDetailsModel.getExperience()));
            designation.setText("Designation: " + checkNull(careTakerCompleteDetailsModel.getDesignation()));
            residenceNumber.setText("Residence number: " + checkNull(careTakerCompleteDetailsModel.getResidence_number()));
            id_addressProof.setText("ID/Address proof: " + checkNull(careTakerCompleteDetailsModel.getId_addressProof()));
            bloodGroup.setText("Blood group: " + checkNull(careTakerCompleteDetailsModel.getBlood_group()));
            father.setText("Father: " + checkNull(careTakerCompleteDetailsModel.getFather()));
            mother.setText("Mother: " + checkNull(careTakerCompleteDetailsModel.getMother()));
            spouse.setText("Spouse: " + checkNull(careTakerCompleteDetailsModel.getSpouse()));
            childre.setText("Children: " + checkNull(careTakerCompleteDetailsModel.getChildren()));
            address.setText("Address: " + checkNull(careTakerCompleteDetailsModel.getAddress()));
            martialStatus.setText("Martial status: " + checkNull(careTakerCompleteDetailsModel.getMartial_status()));

            if (careTakerCompleteDetailsModel.getBackground_verification() != null && !careTakerCompleteDetailsModel.getBackground_verification().isEmpty()) {
                if (careTakerCompleteDetailsModel.getBackground_verification().equalsIgnoreCase("1"))
                    backgroundVerification.setText("Background verification: " + "Verified");
                else if (careTakerCompleteDetailsModel.getBackground_verification().equalsIgnoreCase("0"))
                    backgroundVerification.setText("Background verification: " + "Not Verified");
            }
            Picasso.with(ProductCompleteDetailsActivity.this).load(Config.BASE_URL + careTakerCompleteDetailsModel.getProfile_pic()).placeholder(R.drawable.ic_person).error(R.drawable.ic_person).into(profilePic);

            dialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

            lp.copyFrom(dialog.getWindow().getAttributes());
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int height = displaymetrics.heightPixels;

            int width = displaymetrics.widthPixels;


            dialog.getWindow().setLayout(width, lp.height);
        }
    }

    public void getProjectDetails() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("project_id")) {
            final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(ProductCompleteDetailsActivity.this);
            String project_id = intent.getStringExtra("project_id");
            completed = intent.getBooleanExtra("completed", false);

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);


            Call<ProjectCompletDetailsModel> call = apiService.getCompleteDetailsOfProject(project_id);
            call.enqueue(new Callback<ProjectCompletDetailsModel>() {
                @Override
                public void onResponse(Call<ProjectCompletDetailsModel> call, Response<ProjectCompletDetailsModel> response) {
                    customProgressDialog.cancel();
                    if (response.body() != null && response.body().getError().equalsIgnoreCase("false") && response.body().getTasks() != null && response.body().getTasks().size() > 0 && response.body().getTasks().get(0) != null) {
                        projectCompletDetailsModel = response.body().getTasks().get(0);
                        mainLayout.setVisibility(View.VISIBLE);
                        printProjectDetails(projectCompletDetailsModel);

                    } else {
                        onBackPressed();
                        Toast.makeText(ProductCompleteDetailsActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ProjectCompletDetailsModel> call, Throwable t) {

                    customProgressDialog.cancel();
                    onBackPressed();
                    Toast.makeText(ProductCompleteDetailsActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            onBackPressed();
            Toast.makeText(ProductCompleteDetailsActivity.this, "Someting went wrong", Toast.LENGTH_SHORT).show();
        }

    }


    public void printProjectDetails(ProjectCompletDetailsModel projectCompletDetailsModel) {

        patientName.setText("Name: " + checkNull(projectCompletDetailsModel.getPatient_name()));
        patientAge.setText("Age: " + checkNull(projectCompletDetailsModel.getAge()));
        patientGender.setText("Gender: " + checkNull(projectCompletDetailsModel.getGender()));
        patientHeight.setText("Height: " + checkNull(projectCompletDetailsModel.getHeight()));
        patientWeight.setText("Weight: " + checkNull(projectCompletDetailsModel.getWeight()));
        patientServices.setText("Services: " + checkNull(projectCompletDetailsModel.getServices()));
        hours.setText("Time Period: " + checkNull(projectCompletDetailsModel.getHours()));
        duration.setText("Duration: " + checkNull(projectCompletDetailsModel.getDuration()));
        if (projectCompletDetailsModel.getMoreinfo() != null && !projectCompletDetailsModel.getMoreinfo().isEmpty()) {
            moreinfo.setVisibility(View.VISIBLE);
            moreinfo.setText("More info: " + checkNull(projectCompletDetailsModel.getMoreinfo()));
        } else {
            moreinfo.setVisibility(View.GONE);
        }

        if (projectCompletDetailsModel.getAddress() != null && !projectCompletDetailsModel.getAddress().isEmpty()) {
            address.setVisibility(View.VISIBLE);
            address.setText("Address: " + checkNull(projectCompletDetailsModel.getAddress()));
        } else {
            address.setVisibility(View.GONE);
        }

        if (projectCompletDetailsModel.getCaretaker_name() != null && !projectCompletDetailsModel.getCaretaker_name().isEmpty()) {
            quotes.setVisibility(View.GONE);
            viewQuotes.setVisibility(View.GONE);
            careTakerDetailsTitle.setVisibility(View.VISIBLE);
            careTakerDetailsLayout.setVisibility(View.VISIBLE);
            viewQuotes.setText("Pay Payment");
            careTakerName.setText("Caretaker Name: " + checkNull(projectCompletDetailsModel.getCaretaker_name()));
            organisationName.setText("Organisation Name: " + checkNull(projectCompletDetailsModel.getOrganisation_name()));
            coatedPrice.setText("Coated Price: " + checkNull(projectCompletDetailsModel.getCoated_price()));
            acceptedOn.setText("Accepted On: " + checkNull(converDateTime(projectCompletDetailsModel.getCoated_on())));

        } else {
            if (!projectCompletDetailsModel.getCount().equalsIgnoreCase("0")) {
                quotes.setVisibility(View.VISIBLE);
                viewQuotes.setVisibility(View.GONE);
                careTakerDetailsTitle.setVisibility(View.GONE);
                careTakerDetailsLayout.setVisibility(View.GONE);
                quotes.setText("Quotes quoted: " + checkNull(projectCompletDetailsModel.getCount()));
            } else {
                quotes.setVisibility(View.GONE);
                viewQuotes.setVisibility(View.GONE);
                careTakerDetailsTitle.setVisibility(View.GONE);
                careTakerDetailsLayout.setVisibility(View.GONE);
            }


        }

    }

    public String checkNull(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else return "N/A";
    }

    public String converDateTime(String dateTime) {
        try {


            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = f.parse(dateTime);
            String day = (String) android.text.format.DateFormat.format("dd", date);
            String stringMonth = (String) android.text.format.DateFormat.format("MMM", date);
            String hours = (String) android.text.format.DateFormat.format("HH", date);
            String minutes = (String) android.text.format.DateFormat.format("mm", date);
            Log.d("time", date + "");
            return day + " " + stringMonth + ", " + hours + ":" + minutes;
        } catch (Exception e) {
            return "";
        }
    }

}
