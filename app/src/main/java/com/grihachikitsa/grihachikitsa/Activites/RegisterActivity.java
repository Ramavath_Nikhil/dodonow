package com.grihachikitsa.grihachikitsa.Activites;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.grihachikitsa.grihachikitsa.Fragments.BottomSheetFragment_ChooseImage;
import com.grihachikitsa.grihachikitsa.Fragments.BottomSheetLanguageSelectionFragment;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.RegisterResponseModel;
import com.grihachikitsa.grihachikitsa.Views.CustomProgressDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private Uri fileUri;
    public static int MEDIA_TYPE_IMAGE = 22;
    public static String IMAGE_DIRECTORY_NAME = "grihachikitsa";
    private int PICK_IMAGES = 1;
    final int SELECT_PICTURE = 2, TAKE_PICTURE = 3;
    private Bitmap thumbnail_r;
    private static final int REQUEST_WRITE_STORAGE = 112;
    private CustomProgressDialog progressDialog;
    private CircleImageView profilePic;
    private FloatingActionButton profilePicEdit;
    private EditText email, mobile, first_name, last_name, password, confirmPassword, preferedFirstLanguage, preferedSecondLanguage, preferedThirdLanguage;
    private String imagePath = "";
    private Button createAccount;
    private RelativeLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if (getSupportActionBar() != null) {
            android.support.v7.app.ActionBar bar = getSupportActionBar();
            bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        elementsIntilization();
        onClickListenres();


    }


    public void elementsIntilization() {
        //CIRCULAR IMAGEVIEW INTILIZATION
        profilePic = (CircleImageView) findViewById(R.id.profilepic);

        //FLOATING BUTTON INTILIZATION
        profilePicEdit = (FloatingActionButton) findViewById(R.id.edit);

        //EDITTEXT INTILZATIONS
        first_name = (EditText) findViewById(R.id.firstname);
        last_name = (EditText) findViewById(R.id.lastname);
        email = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.mobile);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirmpassword);
        preferedFirstLanguage = (EditText) findViewById(R.id.firstLanguage);
        preferedSecondLanguage = (EditText) findViewById(R.id.secondLanguage);
        preferedThirdLanguage = (EditText) findViewById(R.id.thirdLanguage);

        //BUTTON INTILIZATION
        createAccount = (Button) findViewById(R.id.createaccount);

        //RELATIVE LAYOUT INTILIATION
        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);

    }


    public void showMessageDialog(String message, final Intent intent, Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_message);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Button ok = (Button) dialog.findViewById(R.id.ok);
        TextView messageTextview = (TextView) dialog.findViewById(R.id.message);
        messageTextview.setText(message);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (intent != null) {
                    startActivity(intent);
                } else {
                    onBackPressed();
                }
            }
        });


        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);
    }


    public void onClickListenres() {

        //EDITEXT ONCLICK LISTENERES
        preferedFirstLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final BottomSheetLanguageSelectionFragment myBottomSheet = BottomSheetLanguageSelectionFragment.newInstance(getLanguages(), "1");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

            }
        });

        preferedSecondLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final BottomSheetLanguageSelectionFragment myBottomSheet = BottomSheetLanguageSelectionFragment.newInstance(getLanguages(), "2");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

            }
        });

        preferedThirdLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final BottomSheetLanguageSelectionFragment myBottomSheet = BottomSheetLanguageSelectionFragment.newInstance(getLanguages(), "3");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());
            }
        });


        //FLOATING BUTTON ONCLIKC LISTENERES
        profilePicEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean hasPermission = (ContextCompat.checkSelfPermission(RegisterActivity.this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

                if (!hasPermission) {
                    ActivityCompat.requestPermissions(RegisterActivity.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                            REQUEST_WRITE_STORAGE);
                } else {

                    final BottomSheetFragment_ChooseImage myBottomSheet = BottomSheetFragment_ChooseImage.newInstance("Modal Bottom Sheet");
                    myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                }

            }
        });

        //BUTTON ONCLICK LISTENERS
        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!first_name.getText().toString().isEmpty()) {
                    if (!last_name.getText().toString().isEmpty()) {
                        if (!email.getText().toString().isEmpty()) {

                            if (!mobile.getText().toString().isEmpty()) {
                                if (!password.getText().toString().isEmpty()) {
                                    if (!confirmPassword.getText().toString().isEmpty()) {

                                        if (!preferedFirstLanguage.getText().toString().isEmpty()) {
                                            if (password.getText().toString().equals(confirmPassword.getText().toString())) {


                                                //  if (imagePath != null && !imagePath.isEmpty())


                                                if (isValidEmail(email.getText().toString())) {
                                                    if (isValidPhoneNumber(mobile.getText().toString())) {
                                                        uploadImage(imagePath);
                                                    } else {
                                                        Toast.makeText(RegisterActivity.this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                                                    }
                                                } else {
                                                    Toast.makeText(RegisterActivity.this, "Please enter valid email address", Toast.LENGTH_SHORT).show();
                                                }




                                               /* else
                                                    Toast.makeText(RegisterActivity.this, "Please provide a profile picture", Toast.LENGTH_SHORT).show();
*/
                                            } else {
                                                Toast.makeText(RegisterActivity.this, "Password and confirm password should match", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(RegisterActivity.this, "Please select prefered first lanugage", Toast.LENGTH_SHORT).show();
                                        }

                                    } else {
                                        Toast.makeText(RegisterActivity.this, "Please enter confirm password", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(RegisterActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(RegisterActivity.this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(RegisterActivity.this, "Please enter valid email", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(RegisterActivity.this, "Please enter your last name", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RegisterActivity.this, "Please enter first name", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private boolean isValidEmail(CharSequence email) {
        if (!TextUtils.isEmpty(email)) {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
        return false;
    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {

        if (phoneNumber.length() < 6 || phoneNumber.length() > 13) {
            return false;
        } else {
            if (!TextUtils.isEmpty(phoneNumber)) {
                return Patterns.PHONE.matcher(phoneNumber).matches();
            }
            return false;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


        // Checks whether a hardware keyboard is available
        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            createAccount.setVisibility(View.GONE);
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {

            createAccount.setVisibility(View.VISIBLE);

        }
    }


    public List<String> getLanguages() {
        List<String> languages = new ArrayList<>();
        languages.add("Hindi");
        languages.add("English");
        languages.add("Bengali");
        languages.add("Telugu");
        languages.add("Marathi");
        languages.add("Tamil");
        languages.add("Urdu");
        languages.add("Kannada");
        languages.add("Gujarati");
        languages.add("Odia");
        languages.add("Malayalam");
        languages.add("Sanskrit");
        return languages;
    }

    public void setText(String type, String selectedText) {
        if (type.equalsIgnoreCase("1")) {
            preferedFirstLanguage.setText(selectedText);

        } else if (type.equalsIgnoreCase("2")) {
            preferedSecondLanguage.setText(selectedText);

        } else if (type.equalsIgnoreCase("3")) {
            preferedThirdLanguage.setText(selectedText);

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void uploadImage(String imagePath) {

        /**
         * Progressbar to Display if you need
         */
        final CustomProgressDialog progressDialog = CustomProgressDialog.show(RegisterActivity.this);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        MultipartBody.Part body = null;
        if (imagePath != null && !imagePath.isEmpty()) {
            //File creating from selected URL
            File file = new File(imagePath);

            // create RequestBody instance from file
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            // MultipartBody.Part is used to send also the actual file name
            body = MultipartBody.Part.createFormData("uploaded_file", file.getName(), requestFile);
        }
        final RequestBody requestEmail =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), email.getText().toString());
        RequestBody requestPassword =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), password.getText().toString());
        RequestBody requestMobile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), mobile.getText().toString());
        RequestBody requestFirstName =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), first_name.getText().toString());
        RequestBody requestLastName =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), last_name.getText().toString());

        RequestBody requestFirstLanguage =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), preferedFirstLanguage.getText().toString());
        RequestBody requestSecondLanguage =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), preferedSecondLanguage.getText().toString());
        RequestBody requestThirdLanugage =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), preferedThirdLanguage.getText().toString());


        Call<RegisterResponseModel> resultCall = apiService.register(body, requestEmail, requestPassword, requestMobile, requestFirstName, requestLastName, requestFirstLanguage, requestSecondLanguage, requestThirdLanugage);
        resultCall.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                progressDialog.cancel();


                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {
                    // Toast.makeText(RegisterActivity.this, response.body().getMessage() + "", Toast.LENGTH_SHORT).show();
                    showMessageDialog(response.body().getMessage(), null, RegisterActivity.this);
                    //  onBackPressed();

                } else {
                    if (response.body() != null && response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(RegisterActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(RegisterActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

                progressDialog.cancel();
                Toast.makeText(RegisterActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public String checkNull(String s)

    {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "";
    }

    public void takePicture() {

        Intent intent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                /*
                 * File photo = new
                 * File(Environment.getExternalStorageDirectory(),
                 * "Pic.jpg"); intent.putExtra(MediaStore.EXTRA_OUTPUT,
                 * Uri.fromFile(photo)); imageUri = Uri.fromFile(photo);
                 */
        // startActivityForResult(intent,TAKE_PICTURE);

        Intent intents = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intents.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intents, TAKE_PICTURE);
    }


    public void selectgImageFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                SELECT_PICTURE);

    }


    // for roted image......
    private Bitmap imageOreintationValidator(Bitmap bitmap, String path) {

        ExifInterface ei;
        try {
            ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateImage(bitmap, 270);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }


    private Bitmap rotateImage(Bitmap source, float angle) {

        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                    source.getHeight(), matrix, true);
        } catch (OutOfMemoryError err) {
            source.recycle();
            Date d = new Date();
            CharSequence s = DateFormat
                    .format("MM-dd-yy-hh-mm-ss", d.getTime());
            String fullPath = Environment.getExternalStorageDirectory()
                    + "/RYB_pic/" + s.toString() + ".jpg";
            if ((fullPath != null) && (new File(fullPath).exists())) {
                new File(fullPath).delete();
            }
            bitmap = null;
            err.printStackTrace();
        }
        return bitmap;
    }


    public static Bitmap decodeSampledBitmapFromResource(String pathToFile,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathToFile, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        Log.e("inSampleSize", "inSampleSize______________in storage"
                + options.inSampleSize);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathToFile, options);
    }


    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

        }

        return inSampleSize;
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
        }
        return list;
    }

    public void startImageSelection() {
        Intent intent = new Intent();

        intent.setType("image/*");


        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Bitmap thePic = null;
                try {
                    thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);


                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    thePic.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    byte[] byteArray = stream.toByteArray();

                  /*  FaceCrop faceCrop = new FaceCrop(this);
                    faceCrop.setFaceCropAsync(profilepic, thePic);*/
                    if (thePic != null) {
                        profilePic.setImageBitmap(thePic);
                    }

                    // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));

                    imagePath = resultUri.getPath();


                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }


        } else {
            switch (requestCode) {



         /*   case Crop.REQUEST_CROP:


                    try {
                        Bundle extras = data.getExtras();

                        Bitmap thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(data));

                        FaceCrop faceCrop = new FaceCrop(this);
                        faceCrop.setFaceCropAsync(profilepic, thePic);

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thePic.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                        byte[] byteArray = stream.toByteArray();

                        progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                        // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
                        uploadDocument(byteArray, Crop.getOutput(data).getPath());
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                break;
*/

                case SELECT_PICTURE:
                    if (resultCode == RESULT_OK && data != null && data.getData() != null) {

                        Uri uri = data.getData();

                        try {
                      /*  thumbnail_r = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        // profilepic.setImageBitmap(thumbnail_r);
                        //updateImage(uri);
                        FaceCrop faceCrop = new FaceCrop(this);

                        faceCrop.setFaceCropAsync(profilepic, thumbnail_r);


                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(thumbnail_r, 700, 700,
                                false);

                        // rotated
                        thumbnail_r = imageOreintationValidator(resizedBitmap,
                                uri.getPath());
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thumbnail_r.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                        // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
                        uploadDocument(byteArray, uri.getPath());*/
                     /*   croppedImageUri = uri;
                        Intent cropIntent = new Intent("com.android.camera.action.CROP");
                        //indicate image type and Uri
                        cropIntent.setDataAndType(uri, "image*//*");
                        //set crop properties
                        cropIntent.putExtra("crop", "true");
                        //indicate aspect of desired crop
                        cropIntent.putExtra("aspectX", 1);
                        cropIntent.putExtra("aspectY", 1);
                        //indicate output X and Y
                        cropIntent.putExtra("outputX", 256);
                        cropIntent.putExtra("outputY", 256);
                        //retrieve data on return
                        cropIntent.putExtra("return-data", true);
                        //start the activity - we handle returning in onActivityResult
                        startActivityForResult(cropIntent, PIC_CROP);*/
                            beginCrop(data.getData());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;


                case TAKE_PICTURE:
                    if (resultCode == RESULT_OK) {

                        previewCapturedImage();

                    }

                    break;


            }
        }


    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        // Crop.of(source, destination).asSquare().start(this);

        CropImage.activity(source)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(16, 16)
                .setRequestedSize(1080, 1080)
                .start(this);

    }

    @SuppressLint("NewApi")
    private void previewCapturedImage() {
        try {
            // hide video preview

            //image.setVisibility(View.VISIBLE);

            // bimatp factory
           /* BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);

            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500,
                    false);

            // rotated
            thumbnail_r = imageOreintationValidator(resizedBitmap,
                    fileUri.getPath());

            //   updateImage(fileUri);


            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            thumbnail_r.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
            progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
            uploadDocument(byteArray, fileUri.getPath());

            FaceCrop faceCrop = new FaceCrop(this);
            faceCrop.setFaceCropAsync(profilepic, thumbnail_r);

            // profilepic.setImageBitmap(thumbnail_r);
            //image.setBackground(null);
            //image.setImageBitmap(thumbnail_r);
            // IsImageSet = true;*/
          /*  croppedImageUri = fileUri;
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(fileUri, "image*//*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);*/
            beginCrop(fileUri);


        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

}
