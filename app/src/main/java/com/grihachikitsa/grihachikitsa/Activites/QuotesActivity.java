package com.grihachikitsa.grihachikitsa.Activites;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.grihachikitsa.grihachikitsa.Adapters.OngoingProjectsAdapter;
import com.grihachikitsa.grihachikitsa.Adapters.QuotesAdapter;
import com.grihachikitsa.grihachikitsa.Models.GetQuotesModel;
import com.grihachikitsa.grihachikitsa.Models.OngoingProjectsModel;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuotesActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private QuotesAdapter mAdapter;
    private List<GetQuotesModel> getQuotesModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes);
        if (getSupportActionBar() != null) {
            android.support.v7.app.ActionBar bar = getSupportActionBar();
            bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Quotes");
        }

        elementsInilization();
        getQuotes();
    }

    public void elementsInilization() {
        //RECYCLERVIEW INTILIAZTION
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(QuotesActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new QuotesAdapter(getQuotesModelList, QuotesActivity.this,QuotesActivity.this);
        recyclerView.setAdapter(mAdapter);


        //PROGRESS BAR INITLIZATION
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
    }


    public void getQuotes() {

        Intent intent = getIntent();

        if (intent != null) {

            OngoingProjectsModel ongoingProjectsModel = (OngoingProjectsModel) intent.getSerializableExtra("projectObject");

            if (ongoingProjectsModel != null && ongoingProjectsModel.getProject_id() != null && !ongoingProjectsModel.getProject_id().isEmpty())

            {
                progressBar.setVisibility(View.VISIBLE);
                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);


                Call<GetQuotesModel> call = apiService.getQuotes(Prefs.getString("apiKey", ""), ongoingProjectsModel.getProject_id());
                call.enqueue(new Callback<GetQuotesModel>() {
                    @Override
                    public void onResponse(Call<GetQuotesModel> call, Response<GetQuotesModel> response) {

                        if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                            if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                                progressBar.setVisibility(View.GONE);
                                getQuotesModelList.addAll(response.body().getTasks());
                                mAdapter.notifyDataSetChanged();
                            } else {
                                progressBar.setVisibility(View.GONE);
                            }

                        } else {
                            progressBar.setVisibility(View.GONE);

                            Log.d("response", "null response");
                        }
                    }

                    @Override
                    public void onFailure(Call<GetQuotesModel> call, Throwable t) {

                        t.printStackTrace();
                        progressBar.setVisibility(View.GONE);

                    }


                });
            } else {
                Toast.makeText(QuotesActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        } else {
            Toast.makeText(QuotesActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
