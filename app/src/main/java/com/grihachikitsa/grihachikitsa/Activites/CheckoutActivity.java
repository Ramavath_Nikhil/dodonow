package com.grihachikitsa.grihachikitsa.Activites;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.grihachikitsa.grihachikitsa.Adapters.CheckoutAdapter;
import com.grihachikitsa.grihachikitsa.Adapters.ProductsAdapter;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.LoginResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProductsResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private CheckoutAdapter mAdapter;
    private LayerDrawable mCartMenuIcon;
    private int mCartCount;
    private ScrollView scrollView;
    private List<ProductsResponseModel> productsResponseModels = new ArrayList<>();
    private Button confirmOrder;
    private double error_count;
    private double totalPrice;
    private TextView customerName, customerEmail, customerMobile, customerAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        }
        elementsInilization();
        getProducts();
        onClickListerners();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(2, intent);
        finish();
    }


    public void elementsInilization() {
        //RECYCLERVIEW INTILIAZTION
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CheckoutActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CheckoutAdapter(productsResponseModels, CheckoutActivity.this, CheckoutActivity.this);
        recyclerView.setAdapter(mAdapter);


        //PROGRESS BAR INITLIZATION
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);


        //SCROLLVIEW INTILIZATION
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        //BUTTON INTILIZATION
        confirmOrder = (Button) findViewById(R.id.confirmOrder);
        confirmOrder.setVisibility(View.GONE);

        //EDITTEXT INTILIZATION
        customerName = (TextView) findViewById(R.id.customerName);
        customerEmail = (TextView) findViewById(R.id.customerEmail);
        customerMobile = (TextView) findViewById(R.id.customerMobile);
        customerAddress = (TextView) findViewById(R.id.address);
    }

    public void onClickListerners() {
        //BUTTON ONCLICK LISTENERS
        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (error_count > 0) {
                    Toast.makeText(CheckoutActivity.this, "Some products are having issue, please check before confirm the order", Toast.LENGTH_SHORT).show();
                } else {
                    confirmDeliveryAddressPopup();
                }
            }
        });
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();

            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return strAdd;
    }

    public void setPrice(double price, double error_count) {
        totalPrice = price;
        confirmOrder.setText("Pay Rs: " + price);
        this.error_count = error_count;
    }

    public void confirmDeliveryAddressPopup() {

        final Dialog dialog = new Dialog(CheckoutActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_confirmaddress);

        final EditText address = (EditText) dialog.findViewById(R.id.address);
        Button next = (Button) dialog.findViewById(R.id.next);

        address.setText(customerAddress.getText().toString());
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (address.getText().toString().isEmpty()) {
                    Toast.makeText(CheckoutActivity.this, "Please provide your address", Toast.LENGTH_SHORT).show();
                } else
                {
                    Intent intent = getIntent();
                    final String latLong = intent.getStringExtra("latlong");
                    startActivity(new Intent(CheckoutActivity.this, PaymentActivity.class).putExtra("totalPrice", totalPrice).putExtra("address",address.getText().toString()).putExtra("latlong",latLong));

                }
                             }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);
    }

    public void getProducts() {

        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Intent intent = getIntent();
        final String latLong = intent.getStringExtra("latlong");

        Call<ProductsResponseModel> call = apiService.getCartProducts(Prefs.getString("apiKey", ""),latLong);
        call.enqueue(new Callback<ProductsResponseModel>() {
            @Override
            public void onResponse(Call<ProductsResponseModel> call, Response<ProductsResponseModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {

                        Intent intent = getIntent();
                        final String latLong = intent.getStringExtra("latlong");

                        final String[] address = {""};
                      /*  Runnable runThis = new Runnable() {
                            @Override
                            public void run() {
                                address[0] = getCompleteAddressString(Double.parseDouble(latLong.split(",")[0]), Double.parseDouble(latLong.split(",")[1]));
                            }


                        };

                        Thread th = new Thread(runThis);
                        th.start();
*/
                     /*   runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                address[0] = getCompleteAddressString(Double.parseDouble(latLong.split(",")[0]), Double.parseDouble(latLong.split(",")[1]));
                                customerAddress.setText(address[0]);
                            }
                        });
*/
                        LoginResponseModel loginResponseModel = new Gson().fromJson(Prefs.getString("user_details", ""), LoginResponseModel.class);
                        if (loginResponseModel != null) {
                            customerName.setText(loginResponseModel.getFirst_name() + " " + loginResponseModel.getLast_name());
                            customerEmail.setText(loginResponseModel.getEmail());
                            customerMobile.setText(loginResponseModel.getMobile());
                        }
                        scrollView.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.VISIBLE);
                        confirmOrder.setVisibility(View.VISIBLE);

                        progressBar.setVisibility(View.GONE);
                        productsResponseModels.addAll(response.body().getTasks());
                        mAdapter.notifyDataSetChanged();

                        if(response.body().getAddress()!=null && !response.body().getAddress().isEmpty())
                            customerAddress.setText(response.body().getAddress());


                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(CheckoutActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(CheckoutActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<ProductsResponseModel> call, Throwable t) {

                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                Toast.makeText(CheckoutActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }


        });
    }
}
