package com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels;

/**
 * Created by Nikil on 12/26/2016.
 */
public class RegisterResponseModel {


    private String error;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
