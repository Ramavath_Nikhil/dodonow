package com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels;

import java.util.List;

/**
 * Created by Nikil on 12/29/2016.
 */
public class CareTakerCompleteDetailsModel {


    private String error, id, organisation_id, caretaker_name, profile_pic, email, mobile, gender, address, age, DOJ, qualification, experience, designation, residence_number, id_addressProof, blood_group, father, mother, martial_status, spouse, children, background_verification, status;

    public List<CareTakerCompleteDetailsModel> getTasks() {
        return tasks;
    }

    public void setTasks(List<CareTakerCompleteDetailsModel> tasks) {
        this.tasks = tasks;
    }

    private List<CareTakerCompleteDetailsModel> tasks;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrganisation_id() {
        return organisation_id;
    }

    public void setOrganisation_id(String organisation_id) {
        this.organisation_id = organisation_id;
    }

    public String getCaretaker_name() {
        return caretaker_name;
    }

    public void setCaretaker_name(String caretaker_name) {
        this.caretaker_name = caretaker_name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDOJ() {
        return DOJ;
    }

    public void setDOJ(String DOJ) {
        this.DOJ = DOJ;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getResidence_number() {
        return residence_number;
    }

    public void setResidence_number(String residence_number) {
        this.residence_number = residence_number;
    }

    public String getId_addressProof() {
        return id_addressProof;
    }

    public void setId_addressProof(String id_addressProof) {
        this.id_addressProof = id_addressProof;
    }

    public String getBlood_group() {
        return blood_group;
    }

    public void setBlood_group(String blood_group) {
        this.blood_group = blood_group;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getMartial_status() {
        return martial_status;
    }

    public void setMartial_status(String martial_status) {
        this.martial_status = martial_status;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getBackground_verification() {
        return background_verification;
    }

    public void setBackground_verification(String background_verification) {
        this.background_verification = background_verification;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
