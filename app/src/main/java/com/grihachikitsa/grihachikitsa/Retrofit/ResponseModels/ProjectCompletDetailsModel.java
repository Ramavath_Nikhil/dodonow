package com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels;

import java.util.List;

/**
 * Created by Nikil on 12/28/2016.
 */
public class ProjectCompletDetailsModel {


    private String error;
    private String id;

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    private String patient_name;
    private String user_id;
    private String gender;
    private String age;
    private String height;
    private String weight;
    private String services;
    private String hours;
    private String duration;
    private String moreinfo;
    private String created_at;
    private String status;
    private String latlong;
    private String address;
    private String caretaker_id, coated_price, coated_on, count, caretaker_name, organisation_name, type;

    public List<ProjectCompletDetailsModel> getTasks() {
        return tasks;
    }

    public void setTasks(List<ProjectCompletDetailsModel> tasks) {
        this.tasks = tasks;
    }

    private List<ProjectCompletDetailsModel> tasks;
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getMoreinfo() {
        return moreinfo;
    }

    public void setMoreinfo(String moreinfo) {
        this.moreinfo = moreinfo;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCaretaker_id() {
        return caretaker_id;
    }

    public void setCaretaker_id(String caretaker_id) {
        this.caretaker_id = caretaker_id;
    }

    public String getCoated_price() {
        return coated_price;
    }

    public void setCoated_price(String coated_price) {
        this.coated_price = coated_price;
    }

    public String getCoated_on() {
        return coated_on;
    }

    public void setCoated_on(String coated_on) {
        this.coated_on = coated_on;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getCaretaker_name() {
        return caretaker_name;
    }

    public void setCaretaker_name(String caretaker_name) {
        this.caretaker_name = caretaker_name;
    }

    public String getOrganisation_name() {
        return organisation_name;
    }

    public void setOrganisation_name(String organisation_name) {
        this.organisation_name = organisation_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
