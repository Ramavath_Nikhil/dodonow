package com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels;

/**
 * Created by Nikil on 1/2/2017.
 */
public class CartCountResponseModel {



    private String cart_count,error;

    public String getCart_count() {
        return cart_count;
    }

    public void setCart_count(String cart_count) {
        this.cart_count = cart_count;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
