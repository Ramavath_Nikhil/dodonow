package com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels;

/**
 * Created by Nikil on 12/24/2016.
 */
public class LoginResponseModel {


    private String error;
    private String first_name;
    private String last_name;
    private String email;
    private String apiKey;
    private String first_language;
    private String second_language;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    private String services ;

    public String getThird_language() {
        return third_language;
    }

    public void setThird_language(String third_language) {
        this.third_language = third_language;
    }

    public String getFirst_language() {
        return first_language;
    }

    public void setFirst_language(String first_language) {
        this.first_language = first_language;
    }

    public String getSecond_language() {
        return second_language;
    }

    public void setSecond_language(String second_language) {
        this.second_language = second_language;
    }

    private String third_language  ;

    public String getLast_project_location() {
        return last_project_location;
    }

    public void setLast_project_location(String last_project_location) {
        this.last_project_location = last_project_location;
    }

    private String last_project_location;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    private String profile_pic;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    private String mobile;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    private String createdAt;
}
