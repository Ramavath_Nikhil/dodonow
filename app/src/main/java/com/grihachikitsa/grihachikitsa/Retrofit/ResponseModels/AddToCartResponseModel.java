package com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels;

/**
 * Created by Nikil on 12/25/2016.
 */
public class AddToCartResponseModel {



    private String error,message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
