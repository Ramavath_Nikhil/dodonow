package com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels;

import java.util.List;

/**
 * Created by Nikil on 1/22/2017.
 */
public class CouponResponseModel {




    private String id;
    private String code;
    private String type;
    private String discount_price;
    private String first_order;
    private String coupon_limit;
    private String min_price;
    private String created_at;
    private String totalprice;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(String discount_price) {
        this.discount_price = discount_price;
    }

    public String getFirst_order() {
        return first_order;
    }

    public void setFirst_order(String first_order) {
        this.first_order = first_order;
    }

    public String getCoupon_limit() {
        return coupon_limit;
    }

    public void setCoupon_limit(String coupon_limit) {
        this.coupon_limit = coupon_limit;
    }

    public String getMin_price() {
        return min_price;
    }

    public void setMin_price(String min_price) {
        this.min_price = min_price;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }

    private String error;

}
