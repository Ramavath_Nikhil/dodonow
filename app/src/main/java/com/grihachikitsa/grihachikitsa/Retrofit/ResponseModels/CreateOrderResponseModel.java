package com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels;

/**
 * Created by Nikil on 1/16/2017.
 */
public class CreateOrderResponseModel {



    private String order_id,order_amount,error,message;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(String order_amount) {
        this.order_amount = order_amount;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}


