package com.grihachikitsa.grihachikitsa.Retrofit;

import android.util.Log;


import com.grihachikitsa.grihachikitsa.Utils.Config;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Nikil on 12/23/2016.
 */

public class ApiClient {


    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {


            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(
                            new Interceptor() {
                                @Override
                                public Response intercept(Interceptor.Chain chain) throws IOException {
                                    Request request = chain.request().newBuilder()
                                            .addHeader("Accept", "Application/JSON").build();
                                    Log.d("request", request.url().toString());
                                    if (request.body() != null) {

                                        Buffer buffer = new Buffer();
                                        request.body().writeTo(buffer);
                                        String body = buffer.readUtf8();
                                        Log.d("body : ", body);
                                    }
                                    Response response = chain.proceed(request);
                                    String strBody = response.body().string();
                                    Log.d("Response", strBody);
                                    return chain.proceed(request);
                                }
                            }).build();


            retrofit = new Retrofit.Builder()
                    .baseUrl(Config.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())

                    .build();
        }
        return retrofit;
    }
}
