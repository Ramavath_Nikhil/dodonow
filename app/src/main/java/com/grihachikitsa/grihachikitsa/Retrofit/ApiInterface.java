package com.grihachikitsa.grihachikitsa.Retrofit;

/**
 * Created by Nikil on 12/23/2016.
 */


import com.grihachikitsa.grihachikitsa.Models.GetQuotesModel;
import com.grihachikitsa.grihachikitsa.Models.OngoingProjectsModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.AddToCartResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.BookCareTakerModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.CareTakerCompleteDetailsModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.CartCountResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.CouponResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.CreateOrderResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.LoginResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.OrdersHistoryModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProductsResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProjectCompletDetailsModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProjectsHistoryResponseModel;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.RegisterResponseModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {

    @Multipart
    @POST("register")
    Call<RegisterResponseModel> register(@Part MultipartBody.Part file,@Part("email") RequestBody email,@Part("password") RequestBody password,@Part("mobile") RequestBody mobile,@Part("first_name") RequestBody first_name,@Part("last_name") RequestBody last_name,@Part("first_languge") RequestBody first_languge,@Part("second_languge") RequestBody second_languge,@Part("third_languge") RequestBody third_languge);


    @Multipart
    @POST("contactus")
    Call<RegisterResponseModel> contactus(@Part MultipartBody.Part file,@Part("type") RequestBody type,@Part("message") RequestBody message,@Part("Authorization") RequestBody Authorization);



    @Multipart
    @POST("updateUserProfilePicture")
    Call<RegisterResponseModel> updateUserProfilePicture(@Part MultipartBody.Part file,@Part("Authorization") RequestBody Authorization);


    @FormUrlEncoded
    @POST("login")
    Call<LoginResponseModel> login(@Field("email") String email , @Field("password") String password);

    @FormUrlEncoded
    @POST("updateUserFCMID")
    Call<RegisterResponseModel> updateUserFCMID(@Field("Authorization") String Authorization , @Field("fcm_id") String fcm_id);
    @FormUrlEncoded
    @POST("deleteItemFromCart")
    Call<RegisterResponseModel> deleteItemFromCart(@Field("Authorization") String Authorization , @Field("cart_id") String cart_id);

    @FormUrlEncoded
    @POST("createproject")
    Call<LoginResponseModel> createProject(@Field("Authorization") String Authorization ,@Field("name") String name, @Field("gender") String gender, @Field("age") String age,@Field("height") String height,@Field("weight") String weight,@Field("services") String services,@Field("hours") String hours,@Field("duration") String duration,@Field("moreinfo") String moreinfo,@Field("latlong") String latLong,@Field("address") String address,@Field("user_name")String user_name,@Field("email") String email);

    @GET("fetchUserDetailsbyMail")
    Call<LoginResponseModel> fetchUserDetailsbyMail(@Query("email") String email);

    @GET("getCompletedProjectsOfUser")
    Call<ProjectsHistoryResponseModel> getCompletedProjectsOfUser(@Query("Authorization") String Authorization);

    @GET("ongoingprojects")
    Call<OngoingProjectsModel> getOngoingProjects(@Query("Authorization") String Authorization);

    @GET("getQuotes")
    Call<GetQuotesModel> getQuotes(@Query("Authorization") String Authorization,@Query("project_id") String project_id);


    @GET("getUserOrders")
    Call<OrdersHistoryModel> getUserOrders(@Query("Authorization") String Authorization);


    @GET("getCareTakerCompleteDetails")
    Call<CareTakerCompleteDetailsModel> getCareTakerCompleteDetails(@Query("id") String id);


    @GET("getRentProducts")
    Call<ProductsResponseModel> getRentProducts(@Query("Authorization") String Authorization,@Query("type") String type);

    @GET("getCartProducts")
    Call<ProductsResponseModel> getCartProducts(@Query("Authorization") String Authorization,@Query("latlong")String latlong);

    @GET("getCompleteDetailsOfProject")
    Call<ProjectCompletDetailsModel> getCompleteDetailsOfProject(@Query("project_id") String project_id);

    @FormUrlEncoded
    @POST("bookCareTaker")
    Call<LoginResponseModel> bookCareTaker(@Field("Authorization") String Authorization , @Field("project_id") String project_id, @Field("caretaker_id") String caretaker_id , @Field("coated_price") String coated_price);

    @FormUrlEncoded
    @POST("updateUserDetails")
    Call<BookCareTakerModel> updateUserDetails(@Field("Authorization") String Authorization , @Field("first_name") String first_name, @Field("last_name") String last_name , @Field("email") String email,@Field("mobile") String mobile,@Field("first_lanugage") String first_lanugage,@Field("second_language") String second_language,@Field("third_language") String third_language);

    @FormUrlEncoded
    @POST("addItemToCart")
    Call<AddToCartResponseModel> addItemToCart(@Field("Authorization") String Authorization , @Field("product_id") String product_id);


    @FormUrlEncoded
    @POST("applycoupon")
    Call<CouponResponseModel> applycoupon(@Field("Authorization") String Authorization , @Field("totalprice") String totalprice, @Field("coupon")String coupon);

    @FormUrlEncoded
    @POST("createOrder")
    Call<CreateOrderResponseModel> createOrder(@Field("Authorization") String Authorization , @Field("payment_type") String payment_type, @Field("address") String address, @Field("latlong") String latlong);


    @FormUrlEncoded
    @POST("getUserCartCount")
    Call<CartCountResponseModel> getUserCartCount(@Field("Authorization") String Authorization );


    @FormUrlEncoded
    @POST("updateCartCountbyRemovingOneItem")
    Call<AddToCartResponseModel> updateCartCountbyRemovingOneItem(@Field("Authorization") String Authorization , @Field("product_id") String product_id);



}
