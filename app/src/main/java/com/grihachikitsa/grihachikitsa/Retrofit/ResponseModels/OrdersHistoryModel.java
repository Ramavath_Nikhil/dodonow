package com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels;

import java.util.List;

/**
 * Created by Nikil on 1/20/2017.
 */
public class OrdersHistoryModel {



    private String id,user_id,total_price,payment_type,address,latlong,created_at,status,error;
    private List<OrdersHistoryModel> tasks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<OrdersHistoryModel> getTasks() {
        return tasks;
    }

    public void setTasks(List<OrdersHistoryModel> tasks) {
        this.tasks = tasks;
    }
}
