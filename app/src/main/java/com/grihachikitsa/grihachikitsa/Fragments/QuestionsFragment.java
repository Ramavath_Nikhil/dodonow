package com.grihachikitsa.grihachikitsa.Fragments;

/**
 * Created by Nikil on 10/5/2016.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.grihachikitsa.grihachikitsa.Activites.MainActivity;
import com.grihachikitsa.grihachikitsa.R;


public class QuestionsFragment extends Fragment implements View.OnClickListener{
    Button next, previous, submit;
    public static QuestionsFragment getInstance(int position){
        QuestionsFragment myFragment = new QuestionsFragment();
        Bundle args = new Bundle();
        args.putInt("position",position);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.layout_fragment_questions, container, false);
        next = (Button) layout.findViewById(R.id.next);
        previous = (Button) layout.findViewById(R.id.previous);
        submit = (Button) layout.findViewById(R.id.submit);
        submit.setOnClickListener(this);
        Bundle bundle = getArguments();
        if (bundle.getInt("position") == 9){

            next.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 2.0f;
            submit.setLayoutParams(params);
        }
        if (bundle.getInt("position") == 0 || bundle.getInt("position") == 1 || bundle.getInt("position") == 2){
            previous.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;
            submit.setLayoutParams(params);
            next.setLayoutParams(params);
        }
        if(bundle.getInt("position") == 0 || bundle.getInt("position") == 1){
            submit.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 2.0f;
            next.setLayoutParams(params);
        }
        return layout;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.submit){
           /* Toast.makeText(getActivity(),"Data submitted",Toast.LENGTH_SHORT).show();
            next.setVisibility(v.GONE);
            previous.setVisibility(v.GONE);*/

            startActivity(new Intent(getActivity(), MainActivity.class));
        }

    }
}

