package com.grihachikitsa.grihachikitsa.Fragments;

/**
 * Created by nikhi on 10/20/2016.
 */

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;


import com.grihachikitsa.grihachikitsa.Activites.ProfileEditActivity;
import com.grihachikitsa.grihachikitsa.Activites.RegisterActivity;
import com.grihachikitsa.grihachikitsa.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MG on 17-07-2016.
 */
public class BottomSheetLanguageSelectionFragment extends BottomSheetDialogFragment {


    private static BottomSheetBehavior mBottomSheetBehavior;
    public List<String> data, languages;
    public String type;

    public static BottomSheetLanguageSelectionFragment newInstance(List<String> data, String type) {
        BottomSheetLanguageSelectionFragment f = new BottomSheetLanguageSelectionFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("data", (ArrayList<String>) data);
        args.putString("type", type);
        f.setArguments(args);

        return f;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = getArguments().getStringArrayList("data");
        type = getArguments().getString("type");

    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        final View v = View.inflate(getContext(), R.layout.layout_fragment_bottomsheet_selectionfragment, null);
        dialog.setContentView(v);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) v.getParent()).getLayoutParams();
        final Button save = (Button) v.findViewById(R.id.save);
        final RecyclerView mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);


        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            mBottomSheetBehavior = (BottomSheetBehavior) behavior;
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {


                    if (newState == 5)
                        getDialog().cancel();

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            if (type.equalsIgnoreCase("language")) {
                v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        v.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        int height = v.getMeasuredHeight();
                        mBottomSheetBehavior.setPeekHeight(height);
                        save.setVisibility(View.VISIBLE);

                    }
                });
            } else {
                save.setVisibility(View.GONE);
            }
        }




        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(new RecyclerView.Adapter<PlanetViewHolder>() {

            @Override
            public PlanetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                    View v = LayoutInflater.from(parent.getContext()).inflate(
                            android.R.layout.simple_list_item_1,
                            parent,
                            false);
                    PlanetViewHolder vh = new PlanetViewHolder(v);
                    return vh;

            }

            @Override
            public void onBindViewHolder(PlanetViewHolder vh, int position) {

                    TextView tv = (TextView) vh.itemView;
                    tv.setText(data.get(position));


            }

            @Override
            public int getItemCount() {
                return data.size();
            }
        });


    }


    private class PlanetViewHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        public PlanetViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


                getDialog().cancel();
                if (type.equalsIgnoreCase("1")) {
                    ((RegisterActivity) getActivity()).setText("1", ((TextView) v).getText().toString());

                } else if (type.equalsIgnoreCase("2")) {
                    ((RegisterActivity) getActivity()).setText("2", ((TextView) v).getText().toString());

                } else if (type.equalsIgnoreCase("3")) {

                    ((RegisterActivity) getActivity()).setText("3", ((TextView) v).getText().toString());
                }
                else if (type.equalsIgnoreCase("4")) {

                    ((ProfileEditActivity) getActivity()).setText("4", ((TextView) v).getText().toString());
                }
                else if (type.equalsIgnoreCase("5")) {

                    ((ProfileEditActivity) getActivity()).setText("5", ((TextView) v).getText().toString());
                }
                else if (type.equalsIgnoreCase("6")) {

                    ((ProfileEditActivity) getActivity()).setText("6", ((TextView) v).getText().toString());
                }


        }
    }


}