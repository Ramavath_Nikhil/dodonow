package com.grihachikitsa.grihachikitsa.Fragments;


import android.support.v4.app.Fragment;
import android.text.TextUtils;


import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.ModelCallbacks;
import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.Page;
import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.ReviewItem;

import java.util.ArrayList;


/**
 * A page asking for a name and an email.
 */
public class CustomerInfoPage extends Page {
    public static final String NAME_DATA_KEY = "More Info";
    public static final String EMAIL_DATA_KEY = "email";

    public CustomerInfoPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return CustomerInfoFragment.create(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem("More Info", mData.getString(NAME_DATA_KEY), getKey()));

    }

    @Override
    public boolean isCompleted() {
        return !TextUtils.isEmpty(mData.getString(NAME_DATA_KEY));
    }
}
