package com.grihachikitsa.grihachikitsa.Fragments;

/**
 * Created by Nikil on 12/6/2016.
 */
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.grihachikitsa.grihachikitsa.Activites.MainActivity;
import com.grihachikitsa.grihachikitsa.Adapters.OngoingProjectsAdapter;
import com.grihachikitsa.grihachikitsa.Models.OngoingProjectsModel;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.LoginResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;
import com.grihachikitsa.grihachikitsa.Views.CustomProgressDialog;
import com.grihachikitsa.grihachikitsa.Views.WizardViewPager.ReviewItem;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;


public class OngoingProjectsFragment extends Fragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private OngoingProjectsAdapter mAdapter;
    private ErrorView errorView;
    private SwipeRefreshLayout swipeRefreshLayout;

    private List<OngoingProjectsModel> ongoingProjectsModelList = new ArrayList<>();
    public OngoingProjectsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_fragment_ongoingprojects, container, false);
        elementsInilization(view);
        getOngoingProjects();
        return view;
    }


    public void elementsInilization(View view)
    {
        //RECYCLERVIEW INTILIAZTION
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new OngoingProjectsAdapter(ongoingProjectsModelList,getActivity());
        recyclerView.setAdapter(mAdapter);


        //PROGRESS BAR INITLIZATION
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

        //ERRORVIEW INTITLIZATION
        errorView = (ErrorView) view.findViewById(R.id.error_view);
        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                errorView.setVisibility(View.GONE);
                getOngoingProjects();
            }
        });

        //SWIPE REFRESH LAYOUT INTILIZATION
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefreshkayout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(true);
                getOngoingProjects();
            }
        });

    }




    public void getOngoingProjects() {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<OngoingProjectsModel> call = apiService.getOngoingProjects(Prefs.getString("apiKey", ""));
        call.enqueue(new Callback<OngoingProjectsModel>() {
            @Override
            public void onResponse(Call<OngoingProjectsModel> call, Response<OngoingProjectsModel> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {





                        if (ongoingProjectsModelList != null && ongoingProjectsModelList.size() > 0) {
                            ongoingProjectsModelList.clear();
                            mAdapter.notifyDataSetChanged();
                        }


                        if(response.body().getTasks()!=null && response.body().getTasks().size()>0)
                    {
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        errorView.setVisibility(View.GONE);
                        ongoingProjectsModelList.addAll(response.body().getTasks());
                        mAdapter.notifyDataSetChanged();
                    }
                    else
                    {

                        progressBar.setVisibility(View.GONE);
                        errorView.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        errorView.setTitle("No ongoing projects");
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    errorView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    errorView.setTitle("Something went wrong");

                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<OngoingProjectsModel> call, Throwable t) {

                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                errorView.setTitle("Something went wrong");

            }


        });

    }



}
