package com.grihachikitsa.grihachikitsa.Fragments;

/**
 * Created by Nikil on 12/6/2016.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.grihachikitsa.grihachikitsa.Adapters.ProjectHistoryAdapter;
import com.grihachikitsa.grihachikitsa.R;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiClient;
import com.grihachikitsa.grihachikitsa.Retrofit.ApiInterface;
import com.grihachikitsa.grihachikitsa.Retrofit.ResponseModels.ProjectsHistoryResponseModel;
import com.grihachikitsa.grihachikitsa.Utils.Prefs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;


public class ProjectsHistoryFragment extends Fragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ProjectHistoryAdapter mAdapter;
    private ErrorView errorView;
    private SwipeRefreshLayout swipeRefreshLayout;

    private List<ProjectsHistoryResponseModel> projectsModelList = new ArrayList<>();
    public ProjectsHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_fragment_projectshistory, container, false);

        elementsInilization(view);
        getCompletedProjects();
        return view;
    }


    public void elementsInilization(View view)
    {
        //RECYCLERVIEW INTILIAZTION
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new ProjectHistoryAdapter(projectsModelList,getActivity());
        recyclerView.setAdapter(mAdapter);


        //PROGRESS BAR INITLIZATION
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

        //ERRORVIEW INTITLIZATION
        errorView = (ErrorView) view.findViewById(R.id.error_view);
        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                errorView.setVisibility(View.GONE);
                getCompletedProjects();
            }
        });

        //SWIPE REFRESH LAYOUT INTILIZATION
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefreshkayout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(true);
                getCompletedProjects();
            }
        });

    }




    public void getCompletedProjects() {

        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ProjectsHistoryResponseModel> call = apiService.getCompletedProjectsOfUser(Prefs.getString("apiKey", ""));
        call.enqueue(new Callback<ProjectsHistoryResponseModel>() {
            @Override
            public void onResponse(Call<ProjectsHistoryResponseModel> call, Response<ProjectsHistoryResponseModel> response) {

                if(projectsModelList!=null && projectsModelList.size()>0)
                {
                    projectsModelList.clear();
                    mAdapter.notifyDataSetChanged();
                }

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    if(response.body().getTasks()!=null && response.body().getTasks().size()>0)
                    {
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        errorView.setVisibility(View.GONE);
                        projectsModelList.addAll(response.body().getTasks());
                        mAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        progressBar.setVisibility(View.GONE);
                        errorView.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        errorView.setTitle("No projects available");
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    errorView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    errorView.setTitle("Something went wrong");

                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<ProjectsHistoryResponseModel> call, Throwable t) {

                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                errorView.setTitle("Something went wrong");

            }


        });

    }



}
