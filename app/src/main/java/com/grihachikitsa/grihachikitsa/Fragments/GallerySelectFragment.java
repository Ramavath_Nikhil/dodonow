package com.grihachikitsa.grihachikitsa.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grihachikitsa.grihachikitsa.R;


/**
 * Created by Nikil on 9/28/2016.
 */
public class GallerySelectFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         View view = inflater.inflate(R.layout.galleryselectfragment,container,false);
        return view;
    }
}
