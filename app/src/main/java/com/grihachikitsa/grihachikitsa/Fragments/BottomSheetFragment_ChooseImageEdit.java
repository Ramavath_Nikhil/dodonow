package com.grihachikitsa.grihachikitsa.Fragments;

/**
 * Created by Nikil on 12/27/2016.
 */

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.TextView;

import com.grihachikitsa.grihachikitsa.Activites.ProfileEditActivity;
import com.grihachikitsa.grihachikitsa.Activites.RegisterActivity;
import com.grihachikitsa.grihachikitsa.R;



/**
 * Created by nikhi on 10/20/2016.
 */

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.TextView;

import com.grihachikitsa.grihachikitsa.Activites.RegisterActivity;
import com.grihachikitsa.grihachikitsa.R;


public class BottomSheetFragment_ChooseImageEdit extends BottomSheetDialogFragment {

    String mString;
    private static BottomSheetBehavior mBottomSheetBehavior;


    public static BottomSheetFragment_ChooseImageEdit newInstance(String string) {
        BottomSheetFragment_ChooseImageEdit f = new BottomSheetFragment_ChooseImageEdit();
        Bundle args = new Bundle();
        args.putString("string", string);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mString = getArguments().getString("string");
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        final View v = View.inflate(getContext(), R.layout.layout_fragment_bottomsheet_chooseimage, null);
        dialog.setContentView(v);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) v.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            mBottomSheetBehavior = (BottomSheetBehavior) behavior;
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            TextView camera = (TextView) v.findViewById(R.id.camera);
            TextView gallery = (TextView) v.findViewById(R.id.gallery);

            camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    getDialog().cancel();
                    ((ProfileEditActivity)getActivity()).takePicture();

                }
            });

            gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().cancel();
                    ((ProfileEditActivity)getActivity()).selectgImageFromGallery();
                }
            });
        }


    }



   /* @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_fragment_locationpreferences, container, false);




        return v;
    }*/
}
